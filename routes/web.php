<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('/',function()
{
   return "this is web application for SKBBL mobile app ";
});
 Auth::routes();
 Route::group(['middleware' => ['auth','permission:1']],function () 
 {
    Route::get('/butwal-home','Admin\OfficeDataController@butwalHome')->name('butwal-home'); 
    Route::get('/butwal-users','Admin\OfficeController@butwalUsers')->name('butwal-users');

    Route::get('/butwal-data','Admin\OfficeDataController@butwalData')->name('butwal-data');
    Route::get('/butwal-alldata','Admin\BackupDataController@btlallData')->name('butwal-alldata');
     Route::get('/butwal-notreceived',
       'Admin\OfficeDataController@butwalNotreceived')->name('butwal-notreceived');
  });
 Route::group(['middleware' => ['auth','permission:3']],function () 
 {
    Route::get('/pokhara-home', 'Admin\OfficeDataController@pkrHome')->name('pokhara-home');
    Route::get('/pokhara-users', 'Admin\OfficeController@pokharaUsers')->name('pokhara-users');
     Route::get('/pokhara-data','Admin\OfficeDataController@pkrData')->name('pokhara-data');
     Route::get('/pokhara-alldata','Admin\BackupDataController@pkrallData')->name('pokhara-alldata');
      Route::get('/pokhara-notreceived',
       'Admin\OfficeDataController@pkrNotreceived')->name('pokhara-notreceived');
 });
 Route::group(['middleware' => ['auth','permission:2']],function () 
 {
    Route::get('/admin-home', 'Admin\OfficeDataController@adminHome')->name('admin-home');
    Route::get('/admin-users', 'Admin\OfficeController@adminUsers')->name('admin-users');
     Route::get('/admin-data', 'Admin\OfficeDataController@adminData')->name('admin-data');
     Route::get('/admin-alldata','Admin\BackupDataController@adminallData')->name('admin-alldata');
     Route::get('/admin-notreceived',
       'Admin\OfficeDataController@adminNotreceived')->name('admin-notreceived');
  });
  Route::group(['middleware' => ['auth','permission:4']],function () 
  { 
    Route::get('/hetauda-home','Admin\OfficeDataController@htdHome')->name('hetauda-home'); 
    Route::get('/hetauda-users','Admin\OfficeController@htdUsers')->name('hetauda-users');

    Route::get('/hetauda-data','Admin\OfficeDataController@htdData')->name('hetauda-data');
    
    Route::get('/hetauda-alldata','Admin\BackupDataController@htdallData')->name('hetauda-alldata');
     Route::get('/hetauda-notreceived',
       'Admin\OfficeDataController@htdNotreceived')->name('hetauda-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:5']],function () 
  { 
    Route::get('/gajuri-home','Admin\OfficeDataController@gjrHome')->name('gajuri-home'); 
    Route::get('/gajuri-users','Admin\OfficeController@gjrUsers')->name('gajuri-users');

    Route::get('/gajuri-data','Admin\OfficeDataController@gjrData')->name('gajuri-data');
    
    Route::get('/gajuri-alldata','Admin\BackupDataController@gjrallData')->name('gajuri-alldata');
     Route::get('/gajuri-notreceived',
       'Admin\OfficeDataController@gjrNotreceived')->name('gajuri-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:6']],function () 
  { 
    Route::get('/bardibas-home','Admin\OfficeDataController@jkrHome')->name('bardibas-home'); 
    Route::get('/bardibas-users','Admin\OfficeController@jkrUsers')->name('bardibas-users');

    Route::get('/bardibas-data','Admin\OfficeDataController@jkrData')->name('bardibas-data');
    
    Route::get('/bardibas-alldata','Admin\BackupDataController@jkrallData')->name('bardibas-alldata');
     Route::get('/bardibas-notreceived',
       'Admin\OfficeDataController@jkrNotreceived')->name('bardibas-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:7']],function () 
  { 
    Route::get('/itahari-home','Admin\OfficeDataController@itrHome')->name('itahari-home'); 
    Route::get('/itahari-users','Admin\OfficeController@itrUsers')->name('itahari-users');

    Route::get('/itahari-data','Admin\OfficeDataController@itrData')->name('itahari-data');
    
    Route::get('/itahari-alldata','Admin\BackupDataController@itrallData')->name('itahari-alldata');
     Route::get('/itahari-notreceived',
       'Admin\OfficeDataController@itrNotreceived')->name('itahari-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:8']],function () 
  { 
    Route::get('/birtamod-home','Admin\OfficeDataController@btmHome')->name('birtamod-home'); 
    Route::get('/birtamod-users','Admin\OfficeController@btmUsers')->name('birtamod-users');

    Route::get('/birtamod-data','Admin\OfficeDataController@btmData')->name('birtamod-data');
    
    Route::get('/birtamod-alldata','Admin\BackupDataController@btmallData')->name('birtamod-alldata');
     Route::get('/birtamod-notreceived',
       'Admin\OfficeDataController@btmNotreceived')->name('birtamod-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:9']],function () 
  { 
    Route::get('/surkhet-home','Admin\OfficeDataController@bdrHome')->name('surkhet-home'); 
    Route::get('/surkhet-users','Admin\OfficeController@bdrUsers')->name('surkhet-users');

    Route::get('/surkhet-data','Admin\OfficeDataController@bdrData')->name('surkhet-data');
    
    Route::get('/surkhet-alldata','Admin\BackupDataController@bdrallData')->name('surkhet-alldata');
     Route::get('/surkhet-notreceived',
       'Admin\OfficeDataController@bdrNotreceived')->name('surkhet-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:10']],function () 
  { 
    Route::get('/attariya-home','Admin\OfficeDataController@attariyaHome')->name('attariya-home'); 
    Route::get('/attariya-users','Admin\OfficeController@atrUsers')->name('attariya-users');

    Route::get('/attariya-data','Admin\OfficeDataController@attariyaData')->name('attariya-data');
    
    Route::get('/attariya-alldata','Admin\BackupDataController@atrallData')->name('attariya-alldata');
     Route::get('/attariya-notreceived',
       'Admin\OfficeDataController@attariyaNotreceived')->name('attariya-notreceived');
  });
   Route::group(['middleware' => ['auth','permission:11']],function () 
  { 
    Route::get('/nepalgunj-home','Admin\OfficeDataController@npjHome')->name('nepalgunj-home'); 
    Route::get('/nepalgunj-users','Admin\OfficeController@npjUsers')->name('nepalgunj-users');

    Route::get('/nepalgunj-data','Admin\OfficeDataController@npjData')->name('nepalgunj-data');
    
    Route::get('/nepalgunj-alldata','Admin\BackupDataController@npjallData')->name('nepalgunj-alldata');
     Route::get('/nepalgunj-notreceived',
       'Admin\OfficeDataController@npjNotreceived')->name('nepalgunj-notreceived');
  });
  Route::group(['middleware' => ['auth','permission:12']],function () 
  { 
    Route::get('/unit-home','Admin\OfficeDataController@unitHome')->name('unit-home'); 
    Route::get('/unit-users','Admin\OfficeController@unitUsers')->name('unit-users');

    Route::get('/unit-data','Admin\OfficeDataController@unitData')->name('unit-data');
    
    Route::get('/unit-alldata','Admin\BackupDataController@unitallData')->name('unit-alldata');
     Route::get('/unit-notreceived',
       'Admin\OfficeDataController@unitNotreceived')->name('unit-notreceived');
  });


Route::group(['middleware' => ['auth']],function () {
  
    Route::prefix('admin')->group(function(){
      Route::get('/changePassword', 'HomeController@showChangePasswordForm');
      Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');
      Route::get('/list', 'Auth\RegisterController@list')->name('users.list');
      Route::post('/import-user','Auth\RegisterController@importOffice')->name('importuser');

    });
  });
   Route::group(['middleware' => ['auth']],function () {

    Route::resource('offices','Admin\OfficeController')->only(['edit', 'update']);
     Route::resource('officedata','Admin\OfficeDataController');
   });
  
  Route::group([ 'namespace' => 'Admin'],function () {
       Route::prefix('admin')->group(function(){
        Route::get('/areadata/{id}','OfficeDataController@databyArea')->name('areadata.show');
       
       
        Route::get('/notreceived','OfficeDataController@notreceived')->name('notreceived');
        Route::get('/received','OfficeDataController@received')->name('received');
        Route::get('/areareceived/{areadata}','OfficeDataController@areareceived')->name('areareceived');
        Route::get('/areanotreceived/{areadata}','OfficeDataController@areanotreceived')->name('areanotreceived');
        Route::post('/get-data','BackupDataController@getdata')->name('get-data');
        Route::get('/all-data','BackupDataController@index')->name('all-data');
        Route::get('/data-index','BackupDataController@allData')->name('data-index');

      });
    });

       //export/import  to excel
    Route::group([ 'namespace' => 'Admin'],function () {
      Route::prefix('admin')->group(function(){
        Route::get('/downlaod-excel','OfficeController@export')->name('downloadinfo');
        Route::get('/export-excel','OfficeDataController@export')->name('exportdata');
        Route::get('/export-received','OfficeDataController@exportreceived')->name('exportreceived');
        Route::get('/export-area/{areadata}','OfficeDataController@exportbyarea')->name('exportbyarea');
        Route::post('/import-office','OfficeController@importData')->name('import');

      });
    });

});
