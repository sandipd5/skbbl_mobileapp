@extends('layouts.gjr-app')
@section('content')
<div class="content-wrapper">
  <div>
           <center><h4> All Coperatives info</h4></center>
     <center><a href = "{{ route('downloadinfo') }}" class="fa fa-download btn btn-success ">Excel</a></center>
        <div class="card-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" class="form-control" required>
                <br>
                <button class="btn btn-success">Import Office Info</button>
            </form>
        </div>
  </div>
  <table id="alldata" class="table table-striped table-bordered" style    ="width:100%">
      <thead>
       <tr style="font-size: 12px;">
        <th>Id</th>
        <th>संस्थाको नाम</th> 
         <th>Username</th>
          <th> Action </th>
         <th>Password</th>
         <th>मोबाईल न.</th>
        <th>संस्थाको प्रकार</th>
        <th>स्थाइ लेखा नंबर</th>
        <th>इलाका कार्यालय</th>
        <th>प्रदेश</th>
        <th>जिल्ला </th>
        <th>पालिका</th>
        <th>पालिकाको प्रकार </th>
        <th>वार्ड नं.</th>
        <th>status</th>
       </tr>
      </thead>
      <tbody>
       
        @foreach($user as $offices)
        <tr style="font-size: 11px;">
        <td>{{ $offices->id }}</td>
        <td>{{ $offices->coperative_name}}</td>
        <td>{{ $offices->user['username'] }}</td>
         <td><a href ="{{ route('offices.edit',$offices->id) }}" ><span class="btn btn-danger btn-sm">EDIT</span></td>
        <td>{{ $offices->user['password'] }}</td>
         <td>{{ $offices->mobile_no }}</td>
        <td>@if($offices->coperative_type == 1)<span>SFCL</span>
            @elseif($offices->coperative_type == 2)<span>Other</span>
            @endif
        </td>
        <td>{{ $offices->pan_no }}</td>
        <td>{{ $offices->area->name }}
        <td>{{ $offices->province }}</td>
        <td>{{ $offices->district }}</td>
        <td>{{ $offices->palika }}</td>
        <td> @if($offices->palika_type == 1)<span>महानगरपालिका</span>
            @elseif($offices->palika_type == 2)<span>उपमहानगरपालिका</span>
              @elseif($offices->palika_type == 3)<span>नगरपालिका</span>
              @elseif($offices->palika_type == 4)<span>गाउँपालिका </span>
              @endif
        </td>
        <td>{{ $offices->ward_no }}</td>
        <td> @if($offices->user['status'] == 1)<span class="btn btn-warning">Active</span>
            @elseif($offices->user['status'] == 0)<span class="btn btn-secondary">Inactive </span>
            @endif
        </td>
        </tr>
        @endforeach
        
      </tbody>
  </table>

</div>
@endsection
