
@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
  <div class="row">
   <div class = "center" style="color:red;width:30%">
      <h5>Data Summary of {{ $nyear }}
           @if($nmonth == "1" ) Baisakh
           @elseif($nmonth == "2") Jestha
           @elseif($nmonth == "3") Ashar
           @elseif($nmonth == "4") Shrawan
           @elseif($nmonth == "5") Bhadra
           @elseif($nmonth == "6") Ashoj
           @elseif($nmonth == "7") Kartik
           @elseif($nmonth == "8") Mangsir
           @elseif($nmonth == "9") Poush
           @elseif($nmonth == "10") Magh
           @elseif($nmonth == "11") Falgun
           @elseif($nmonth == "12") Chait
           @endif </h5>


      </br>
   </div>

     <div class="col-sm-12">
      <div class="card text-white bg-secondary mb-3" style="max-width: 14rem;max-height: 10rem;margin-left:423px;">
          <div class="card-header">Headoffice</div>
          <div class="card-body" style="color:black; background-color:coral;">
            <h6 class="card-title">Total coperatives : {{ $office->count() }}</h6>
            <h6 class="card-text">Received : {{ $officedata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $office->count() - $officedata->count() }}</h6>
          </div>
         </div>
        </div>
     </br> </br> </br>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Attariya</div>
          <div class="card-body" style="color:black; background-color:lightgreen;">
            <h6 class="card-title">Total coperatives :{{ $attariyaoffice->count() }} </h6>
            <h6 class="card-text">Received : {{ $attariyadata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $attariyaoffice->count() -  $attariyadata->count() }}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Butwal</div>
          <div class="card-body" style="color:black; background-color:lightblue;">
            <h6 class="card-title">Total coperatives : {{ $butwaloffice->count() }}</h6>
            <h6 class="card-text">Received : {{ $butwaldata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $butwaloffice->count() - $butwaldata->count()}}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Hetauda</div>
          <div class="card-body" style="color:black; background-color:honeydew;">
            <h6 class="card-title">Total coperatives : {{ $hetaudaoffice->count() }}</h6>
            <h6 class="card-text">Received : {{ $hetaudadata->count() }}</h6>
            <h6 class="card-text">Pending :{{ $hetaudaoffice->count() - $hetaudadata->count() }}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Birtamod</div>
          <div class="card-body" style="color:black; background-color:hotpink;">
            <h6 class="card-title">Total coperatives : {{ $birtamodoffice->count() }}</h6>
            <h6 class="card-text">Received : {{ $birtamoddata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $birtamodoffice->count() - $birtamoddata->count()}}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Itahari</div>
          <div class="card-body" style="color:black; background-color:indianred;">
            <h6 class="card-title">Total coperatives : {{ $itaharioffice->count() }}</h6>
            <h6 class="card-text">Received : {{ $itaharidata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $itaharioffice->count() - $itaharidata->count() }}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Pokhara</div>
          <div class="card-body" style="color:black; background-color:khaki;">
            <h6 class="card-title">Total coperatives : {{ $pokharaoffice->count() }}</h6>
            <h6 class="card-text">Received : {{ $pokharadata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $pokharaoffice->count() - $pokharadata->count()}}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Birendranagar</div>
          <div class="card-body" style="color:black; background-color:lavender;">
            <h6 class="card-title">Total coperatives : {{ $birendranagaroffice->count() }}</h6>
            <h6 class="card-text">Received : {{ $birendranagardata->count() }}</h6>
            <h6 class="card-text">Pending :{{ $birendranagaroffice->count() -  $birendranagardata->count()}}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Gajuri</div>
          <div class="card-body" style="color:black; background-color:lawngreen;">
            <h6 class="card-title">Total coperatives :{{ $gajurioffice->count() }}</h6>
            <h6 class="card-text">Received :{{ $gajuridata->count() }}</h6>
            <h6 class="card-text">Pending : {{ $gajurioffice->count() -  $gajuridata->count()}}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Nepalgunj</div>
          <div class="card-body" style="color:black; background-color:navajowhite;">
            <h6 class="card-title">Total coperatives :{{ $nepalganjoffice->count() }}</h6>
            <h6 class="card-text">Received :{{ $nepalganjdata->count() }}</h6>
            <h6 class="card-text">Pending :{{ $nepalganjoffice->count() - $nepalganjdata->count() }}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Janakpur</div>
          <div class="card-body" style="color:black; background-color:violet;">
            <h6 class="card-title">Total coperatives :{{ $janakpuroffice->count() }}</h6>
            <h6 class="card-text">Received :{{ $janakpurdata->count() }}</h6>
            <h6 class="card-text">Pending :{{ $janakpuroffice->count() - $janakpurdata->count()}}</h6>
          </div>
         </div>
    </div>

    <div class="col-sm-3">
      <div class="card text-white bg-primary mb-3" style="max-width: 14rem;max-height: 10rem;">
          <div class="card-header">Unit</div>
          <div class="card-body" style="color:black; background-color:thistle;">
            <h6 class="card-title">Total coperatives :{{ $unitoffice->count() }}</h6>
            <h6 class="card-text">Received :{{ $unitdata->count() }}</h6>
            <h6 class="card-text">Pending :{{ $unitoffice->count() - $unitdata->count()}}</h6>
          </div>
         </div>
    </div>

  </div>
 </div>
 </div>
@endsection
