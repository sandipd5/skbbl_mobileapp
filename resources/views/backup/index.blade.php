@extends('layouts.app')
@section('content')
<div class="content-wrapper" >
<table id="table" class="table table-bordered" style="width: 100%" ; border="none" >
    <thead>
      <tr>
        <th>Id</th>
        <th>fiscal year</th>
        <th>month</th>
        <th>officename</th>
        <th>area</th>
        <th>District</th>
        <th>member dalit male</th>
        <th>member dalit female</th>
        <th>member janajati male</th>
        <th>member janajati female</th>
        <th>member other male</th>
        <th>member other female</th>
        <th>total member</th>
        <th>loaned male number</th>
        <th>loaned female number</th>
        <th>total loaned people</th>
        <th>laganima raheko rakam</th>
        <th>lagani</th>
        <th>sewa asuli</th>
        <th>byaj asuli</th>
        <th>vaka nageko rakam</th>
        <th>paunu parne baki byaj</th>
        <th>antarik source share pungi</th>
        <th>antarik source jageda kosh</th>
        <th>antarik source samuha bachat</th>
        <th>antarik source other</th>
        <th>total antarik source pungi</th>
        <th>skbbl bata lieko loan</th>
        <th>skbbl lai bujaeko loan</th>
        <th>skbbl lai tirna baki loan</th>
        <th>aru bata lieko loan</th>
        <th>aru lai bujaeko loan</th>
        <th>aru lai tirna baki loan</th>
        <th>total external loan</th>
        <th>byaj income</th>
        <th>other income</th>
        <th>total income</th>
        <th>byaj expense</th>
        <th>karmachari parsasanik expense</th>
        <th>jokhim kosh expense</th>
        <th>total expense</th>
        <th>profit loss</th>

      </tr>
    </thead>

</table>
</div>
@endsection
