<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sana Kisan</title>
  <!-- Bootstrap core CSS-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- Custom fonts for this template-->
  <link href="{!! asset('vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

  <!-- Custom styles for this template-->
  <link href="{!! asset('css/sb-admin.css') !!}" rel="stylesheet">
  <style>

  .center
  {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 10%;
  }
  a.btn:hover {
     -webkit-transform: scale(1.1);
     -moz-transform: scale(1.1);
     -o-transform: scale(1.1);
 }
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
  </style>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <a class="navbar-brand" href=#>Sana Kisan Bikas Bank Butwal</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion" style="overflow-x:scroll;">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="{{ route('butwal-home') }}">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">Dashboard</span>
            </a>
          </li>
       <!--   <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="{{route('users.list')}}">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">Users</span>
            </a>
          </li> -->
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">

            <a class="nav-link" href="{{ route('butwal-users') }}">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">Coperatives</span>
            </a>
            
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="{{ route('butwal-data') }}">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">Active Data</span>
            </a>
          </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="{{ route('butwal-alldata') }}">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">All-Data</span>
            </a>
        </li>
        </ul>

        <ul class="navbar-nav sidenav-toggler">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <form class="form-inline my-2 my-lg-0 mr-lg-2">
              <div class="input-group">
                <input class="form-control" type="text" placeholder="Search for...">
                <span class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </form>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
              <i class="fa fa-fw fa-sign-out"></i>Logout</a>
          </li>
        </ul>
      </div>
  </nav>
       <!-- content goes here  -->

      @yield('content')

     <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright © Sana Kisan Bikas Bank 2019</small>
          </div>
        </div>
    </footer>
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>
      <!-- Logout Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                                        <a class="btn btn-primary" href="{{ route('logout') }}"
                                              onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                                              Logout
                                          </a>

                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                              {{ csrf_field() }}
                                          </form>
                                            <li>
                                              <a href="{{ route('changePassword') }}">
                                                 Change Password
                                              </a>
                                          </li>
           </div>
          </div>
        </div>
      </div>
      <!--delete modal -->
      <div class="modal" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you, want to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
      </div>
      <!-- Bootstrap core JavaScript-->


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="{!! asset('js/popper.min.js') !!}"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <script  type="text/javascript" src="{!! asset('js/sb-admin.min.js') !!}"></script>
      <script type="text/javascript" src="{{ asset('js/fastclick.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
       <!-- <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script> -->

      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src=" https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src= "https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>

      <script>
            $(document).ready(function() {
            var table = $('#example').DataTable( {
                scrollY:        "400px",
                scrollX:        true,
                scrollCollapse: true,
                dom: '<"top"Bf>rt<"bottom"lip><"clear">',
                buttons: ['excel']
             });
           });
      </script>
      <script>
            $(document).ready(function() {
            var table = $('#alldata').DataTable( {
                scrollY:        "400px",
                scrollX:        true,
                scrollCollapse: true,
                dom: '<"top"Bf>rt<"bottom"lip><"clear">',
                buttons: ['excel']

             });
           });
      </script>
      <script>
      $(document).ready( function () {
        $('#table').DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": '{{ route("get-data") }}',
            "type": "POST",

            "data": {
                   _token: '{!! csrf_token() !!}',
                 },
            },
           "scrollY":        true,
           "scrollX":        true,
           "scrollCollapse": true,


          "columns": [
                        { data: 'id'},
                         { data: 'fiscal_year'},
                         { data: 'month' },
                         { data: 'officename'},
                         { data:  'area'},
                         { data: 'district'},
                         { data: 'member_dalit_male'  },
                         { data: 'member_dalit_female' },
                         { data: 'member_janajati_male' },
                         { data: 'member_janajati_female' },
                         { data: 'member_other_male' },
                         { data: 'member_other_female' },
                         { data: 'total_member' },
                         { data: 'loaned_male_number' },
                         { data: 'loaned_female_number' },
                         { data: 'total_loaned_people' },
                         { data: 'laganima_raheko_rakam' },
                         { data: 'lagani' },
                         { data: 'sewa_asuli' },
                         { data: 'byaj_asuli' },
                         { data: 'vaka_nageko_rakam' },
                         { data: 'paunu_parne__baki_byaj'},
                         { data: 'antarik_source_share_pungi' },
                         { data: 'antarik_source_jageda_kosh' },
                         { data: 'antarik_source_samuha_bachat' },
                         { data: 'antarik_source_other' },
                         { data: 'total_antarik_source_pungi' },
                         { data: 'skbbl_bata_lieko_loan' },
                         { data: 'skbbl_lai_bujaeko_loan' },
                         { data: 'skbbl_lai_tirna_baki_loan' },
                         { data: 'aru_bata_lieko_loan' },
                         { data: 'aru_lai_bujaeko_loan' },
                         { data: 'aru_lai_tirna_baki_loan' },
                         { data: 'total_external_loan' },
                         { data: 'byaj_income' },
                         { data: 'other_income' },
                         { data: 'total_income' },
                         { data: 'byaj_expense'},
                         { data: 'karmachari_parsasanik_expense' },
                         { data: 'jokhim_kosh_expense' },
                         { data: 'total_expense' },
                         { data: 'profit_loss' },

          ],

        });
      } );
      </script>
      <script>
        $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
        e.preventDefault();
        var $form=$(this);
        $('#confirm').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete-btn', function(){
                $form.submit();
            });
        });
      </script>
