@extends('layouts.app')
@section('content')
<div class="content-wrapper">
   
 <main class="my-form">
    <div class="cotainer">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card">
            <div class="card-body">
                <form name="my-form" action="{{  route('offices.update',$office->id) }}" method="post"       enctype="multipart/form-data">  
                      {{ csrf_field() }}
                       {{ method_field('PATCH') }}
 					 <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Coperative Name :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="coperative_name" value="{{ $office->coperative_name }}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Username :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="username" value="{{ $office->user['username'] }}" required>
                          </div>
                     </div>
                      <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Password :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="password" value="{{ $office->user['password'] }}" required>
                          </div>
                     </div>

                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Mobile No. :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="mobile_no" value="{{ $office->mobile_no }}">
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Pan No :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="pan_no" value="{{ $office->pan_no }}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Province :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="province" value="{{ $office->province }}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">District :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="district" value="{{ $office->district }}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Palika :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="palika" value="{{ $office->palika }}" required>
                          </div>
                     </div>
                     <div class="col-md-8 offset-md-4">
                       <button type="submit" class="btn btn-primary">
                       	    Submit</button>
                       	    <a href="{{ URL::previous() }}"><span class="btn btn-danger btn-sm">CANCEL</span></a>
                     </div>
                </form>
            </div>
           </div>
          </div>
        </div>
     </div>                          
  </main>
</div>
@endsection