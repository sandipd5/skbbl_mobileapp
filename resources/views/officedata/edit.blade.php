@extends('layouts.app')
@section('content')
<div class="content-wrapper">
 <main class="my-form">
    <div class="cotainer">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card">
            <div class="card-body">
                <form name="my-form" action="{{  route('officedata.update',$officedata->id) }}" method="post"       enctype="multipart/form-data">  
                      {{ csrf_field() }}
                       {{ method_field('PATCH') }}
 					            <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Fiscal Year:</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="fiscal_year" value="{{ $officedata->fiscal_year }}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">Coperative Name :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="coperative_name" value="{{ $officedata->office['coperative_name']}}" disabled required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                       		text-lg-right">Month :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="month" value="{{ $officedata->month}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">member dalit male :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="member_dalit_male" value="{{ $officedata->member_dalit_male}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">member dalit female :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="member_dalit_female" value="{{  $officedata->member_dalit_female}}" required>
                          </div>
                     </div>
                      <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">member janajati male :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="member_janajati_male" value="{{ $officedata->member_janajati_male}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">member janajati female :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="member_janajati_female" value="{{ $officedata->member_janajati_female}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">member other male :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="member_other_male" value="{{ $officedata->member_other_male}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">member other female :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="member_other_female" value="{{ $officedata->member_other_female}}" required>
                          </div>
                     </div>
                    
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">total member :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="total_member" value="{{ $officedata->total_member}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">loaned male number :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="loaned_male_number" value="{{ $officedata->loaned_male_number}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">loaned female number :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="loaned_female_number" value="{{ $officedata->loaned_female_number}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">total loaned people :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="total_loaned_people" value="{{ $officedata->total_loaned_people}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">lagani :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="lagani" value="{{ $officedata->lagani}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">sawa asuli :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="sewa_asuli" value="{{ $officedata->sewa_asuli}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">laganima raheko rakam :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="laganima_raheko_rakam" value="{{ $officedata->laganima_raheko_rakam}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">byaj asuli :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="byaj_asuli" value="{{ $officedata->byaj_asuli}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">vaka nageko rakam :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="vaka_nageko_rakam" value="{{ $officedata->vaka_nageko_rakam}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">paunu parne baki byaj :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="paunu_parne__baki_byaj" value="{{ $officedata->paunu_parne__baki_byaj}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">antarik shrot share puji:</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="antarik_source_share_pungi" value="{{ $officedata->antarik_source_share_pungi}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">antarik shrot jageda kosh :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="antarik_source_jageda_kosh" value="{{ $officedata->antarik_source_jageda_kosh}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">antarik shrot samuha bachat :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="antarik_source_samuha_bachat" value="{{ $officedata->antarik_source_samuha_bachat}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">antarik shrot other :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="antarik_source_other" value="{{ $officedata->antarik_source_other}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">total antarik shrot :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="total_antarik_source_pungi" value="{{ $officedata->total_antarik_source_pungi}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">skbbl bata liyeko loan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="skbbl_bata_lieko_loan" value="{{ $officedata->skbbl_bata_lieko_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">skbbl lai bujaeko loan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="skbbl_lai_bujaeko_loan" value="{{ $officedata->skbbl_lai_bujaeko_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">skbbl lai tirna baki loan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="skbbl_lai_tirna_baki_loan" value="{{ $officedata->skbbl_lai_tirna_baki_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">aru bata liyeko loan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="aru_bata_lieko_loan" value="{{ $officedata->aru_bata_lieko_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">aru lai bujaeko loan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="aru_lai_bujaeko_loan" value="{{ $officedata->aru_lai_bujaeko_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">aru lai tirna bakiloan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="aru_lai_tirna_baki_loan" value="{{ $officedata->aru_lai_tirna_baki_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">total external loan :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="total_external_loan" value="{{ $officedata->total_external_loan}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">byaj amdani :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="byaj_income" value="{{ $officedata->byaj_income}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">anya amdani :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="other_income" value="{{ $officedata->other_income}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">total amdani :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="total_income" value="{{ $officedata->total_income}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">byaj kharcha :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="byaj_expense" value="{{ $officedata->byaj_expense}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">karmachari/parsasanik kharcha :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="karmachari_parsasanik_expense" value="{{ $officedata->karmachari_parsasanik_expense}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">anya  kharcha :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="jokhim_kosh_expense" value="{{ $officedata->jokhim_kosh_expense}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">total kharcha :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="total_expense" value="{{ $officedata->total_expense}}" required>
                          </div>
                     </div>
                     <div class="form-group row">
                         <label for="" class="col-md-4 col-form-label 
                          text-lg-right">Profit Loss :</label>
                          <div class="col-md-6">
                             <input type="text" id="" class="form-control" name="profit_loss" value="{{ $officedata->profit_loss}}" required>
                          </div>
                     </div>
                     <div class="col-md-8 offset-md-4">
                       <button type="submit" class="btn btn-primary">
                       	    UPDATE</button>

                       	    <a href="{{ URL::previous() }}"><span class="btn btn-success btn-md">CANCEL</span></a>

                     </div>
                </form>
                <form action="{{ route('officedata.destroy',$officedata->id)}}" method="post" style="margin-left: 270px">
                  {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button class="btn btn-danger btn-lg delete" type="submit" name = "delete_modal"><strong>Delete</strong></button>
                </form>            
            </div>
           </div>
          </div>
        </div>
     </div>                          
  </main>
</div>
@endsection