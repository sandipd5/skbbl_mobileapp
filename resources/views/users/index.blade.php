@extends('layouts.app')
@section('content')
<div class="content-wrapper" >
   <div class="card-body">
            <form action="{{ route('importuser') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" class="form-control" required>
                <br>
                <button class="btn btn-success">Import User Data</button>
            </form>
   </div>
  <table id="example" class="table table-striped table-bordered" style="width:100%">
      <thead>
       <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Email</th>
        <th>Coperative</th>
        <th>role</th>
        <th>area</th>
        <th>Status</th>
       </tr>
      </thead>
      <tbody>
         @foreach($users as $user)
          <tr>
           <td>{{ $user->id }}
           <td>{{ $user->name }}</td>
           <td>{{ $user->email }}</td>
           <td>{{ $user->office['coperative_name'] }}
            <td>{{ $user->role}}</td>
            <td> {{ $user->office['area_offfice_id'] }} </td>
           <td>  @if($user->status == 1)<span class="btn btn-warning">Active</span>
              @elseif($user->status == 0)<span class="btn btn-secondary">Inactive </span>
              @endif
           </td>
          </tr>
         @endforeach

      </tbody>
    </table>
    <div class="text-center">
       <a href="{{route('changePassword')}}" class="btn btn-warning">Change Password of logged User</a>
    </div>
</div>
@endsection
