<table>
     <thead>
       <tr>
       <th>क्र.स</th>
       <th>आर्थिक बर्ष</th>
       <th>महिना</th>
       <th>संस्था</th>
       <th>ईलाका कार्यालय</th>
       <th>दलित पुरुष सदस्य</th>
       <th>दलित महिला सद्स</th>
       <th>जनजाती पुरुष सद्स्य</th>
       <th>जनजाती महिला सद्स्य </th>
       <th>अन्य पुरुष सद्स्य </th>
       <th>अन्य महिला सद्स्य </th>
       <th>जम्मा सद्स्य</th>
       <th>ऋणी पुरुष सदस्य  </th>
       <th>ऋणी महिला सदस्य </th>
       <th>जम्मा ऋणी सदस्य </th>
       <th>ल . र .र </th>
       <th>लगानी </th>
       <th>सावा असुली  </th>
       <th>ब्याज असुली </th>
       <th>भाका नाघेको रकम </th>
       <th>पाउनु पर्ने बाकी ब्याज</th>
       <th>आन्तरिक श्रोत सेयर पुजी </th>
       <th>आन्तरिक श्रोत जगेडा कोस   </th>
       <th>आन्तरिक श्रोत समुह बचत  </th>
       <th>आन्तरिक श्रोत अन्य </th>
       <th>जम्मा आन्तरिक श्रोत पुजी </th>
       <th>साना किसान बैंक बाट लिएको ऋण</th>
       <th>साना किसान बैंक लाई बुझाएको ऋण </th>
       <th>साना किसान बैंक लाई तिर्न बाँकी  ऋण </th>
       <th>अन्य बाट लिएको  ऋण </th>
       <th>अन्य लाई बुझाएको ऋण </th>
       <th>अन्य लाई तिर्न बाँकी ऋण </th>
       <th> जम्मा ऋण </th>
       <th>ब्याज आमदानी</th>
       <th>अन्य आमदानी </th>
       <th> जम्मा आम्दानी </th>
       <th>ब्याज खर्च </th>
       <th>कर्मचारी प्रसासनिक खर्च </th>
       <th>जोखिम कोश खर्च </th>
        <th> जम्मा खर्च</th>
       <th> नाफा नोक्सान</th>
      </tr>
     </thead>
     <tbody>
     @foreach($officedata as $datas)
       <tr>
        <td> {{ $datas->id }}</td>
        <td>{{ $datas->fiscal_year }}</td>
        <td>{{ $datas->month }}</td>
        <td>{{ $datas->office->coperative_name }}</td>
        <td>{{ $datas->area->name }}</td>
        <td>{{ $datas->member_dalit_male }}</td>
        <td>{{ $datas->member_dalit_female }}</td>
        <td>{{ $datas->member_janajati_male }}</td>
        <td>{{ $datas->member_janajati_female }}</td>
        <td>{{ $datas->member_other_male }}</td>
        <td>{{ $datas->member_other_female }}</td>
        <td>{{ $datas->total_member }}</td>
        <td>{{ $datas->loaned_male_number }}</td>
        <td>{{ $datas->loaned_female_number }}</td>
        <td>{{ $datas->total_loaned_people }}</td>
        <td>{{ $datas->laganima_raheko_rakam }}</td>
        <td>{{ $datas->lagani }}</td>
        <td>{{ $datas->sewa_asuli }}</td>
        <td>{{ $datas->byaj_asuli }}</td>
        <td>{{ $datas->vaka_nageko_rakam }}</td>
        <td>{{ $datas->paunu_parne__baki_byaj }}</td>
        <td>{{ $datas->antarik_source_share_pungi }}</td>
        <td>{{ $datas->antarik_source_jageda_kosh }}</td>
        <td>{{ $datas->antarik_source_samuha_bachat }}</td>
        <td>{{ $datas->antarik_source_other }}</td>
        <td>{{ $datas->total_antarik_source_pungi }}</td>
        <td>{{ $datas->skbbl_bata_lieko_loan}}</td>
        <td>{{ $datas->skbbl_lai_bujaeko_loan }}</td>
        <td>{{ $datas->skbbl_lai_tirna_baki_loan }}</td>
        <td>{{ $datas->aru_bata_lieko_loan }}</td>
        <td>{{ $datas->aru_lai_bujaeko_loan }}</td>
        <td>{{ $datas->aru_lai_tirna_baki_loan }}</td>
        <td>{{ $datas->total_external_loan }}</td>
        <td>{{ $datas->byaj_income }}</td>
        <td>{{ $datas->other_income }}</td>
        <td>{{ $datas->total_income }}</td>
        <td>{{ $datas->byaj_expense }}</td>
        <td>{{ $datas->karmachari_parsasanik_expense }}</td>
        <td>{{ $datas->jokhim_kosh_expense }}</td>
        <td>{{ $datas->total_expense }}</td>
        <td>{{ $datas->profit_loss }}</td>

     </tr>
     @endforeach
     </tbody>
</table>
