@extends('layouts.app')
@section('content')
<div class="content-wrapper";>
 <div>
  <center>
   <span><strong><?php echo date('F') ?></strong></span>
   <span><strong><?php echo date('Y') ?></strong></span>
   <br/><br/>
  </center>
 </div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
      <thead>
       <tr>
        <th>Id</th>
        <th>Coperative</th>
        <th>Area Office</th>
        <th>Mobile Number</th>
        <th>Status</th>


       </tr>
      </thead>
      <tbody>
      @foreach($notreceived as $datas)
        <tr>
         <td> {{ $datas->id }}</td>
         <td>{{ $datas->coperative_name }}</td>
         <td>{{ $datas->area->name }} </td>
         <td>{{ $datas->mobile_no  }} </td>
         <td><span class="btn btn-secondary">Inactive</span></td>



      </tr>
      @endforeach
      </tbody>
  </table>
</div>
@endsection
