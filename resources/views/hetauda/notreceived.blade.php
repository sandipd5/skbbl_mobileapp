@extends('layouts.htd-app')
@section('content')
<div class="content-wrapper">
 
<table id="example" class="table table-striped table-bordered" style="width:100%">
      <thead>
       <tr>
        <th>Id</th>
        <th>Coperative </th>
        <th>status</th>
        <th>Mobile Number</th>
        <th>Area</th>
       </tr>
      </thead>
      <tbody>
      @foreach($notreceived as $datas)
        <tr>
         <td> {{ $datas->id }}</td>
         <td>{{ $datas->coperative_name }}</td>
         <td><span class="btn btn-secondary">Inactive</span></td>
         <td>{{ $datas->mobile_no }}</td>
         <td>{{ $datas->area->name }}</td>
      </tr>
      @endforeach
      </tbody>
  </table>
</div>
@endsection
