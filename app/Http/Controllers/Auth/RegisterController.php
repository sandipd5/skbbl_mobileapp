<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Area;
use App\Imports\UsersImport;
use Excel;

class RegisterController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }


   public function list()
    {
       $users=User::get();
       $area = Area::get();
       return view('users.index',compact('users','area'));

    }
   



   public function importOffice()
   {
    Excel::import(new UsersImport,request()->file('file'));
    return back(); 
   } 
}
