<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

   
    
    
   public function redirectTo()
    {
          $role = Auth::user()->role; 
          switch ($role) {
            case '1':
              return '/butwal-home';
              break;
            case '2':
              return '/admin-home';
              break;  
            case '3':
              return '/pokhara-home';
              break; 
            case '4':
              return '/hetauda-home';
              break; 
            case '5':
              return '/gajuri-home';
              break;
            case '6':
              return '/bardibas-home';
              break;  
            case '7':
              return '/itahari-home';
              break;    
            case '8':
              return '/birtamod-home';
              break;   
            case '9':
              return '/surkhet-home';
              break; 
            case '10':
              return '/attariya-home';
              break;
            case '11':
              return '/nepalgunj-home';
              break; 
            case '12':
              return '/unit-home';
              break;          

          }
    }
    
    public function __construct()
    {
        
        $this->middleware('guest')->except('logout');
    }
    
}
