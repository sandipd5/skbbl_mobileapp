<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\DataExport;
use App\Exports\ReceivedExport;
use App\Exports\AreadataExport;
use App\Officedata;
use App\Area;
use App\Office;
use Auth;
use Excel;


class OfficeDataController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');

    }
    public function butwalHome()
       {
         return view('butwal.home');
       }
    public function attariyaHome()
       {
         return view('attariya.home');
       }  
    public function itrHome()
       {
         return view('itr.home');
       }    
    public function pkrHome()
       {
         return view('pokhara.home');
       }
    public function jkrHome()
       {
         return view('jkr.home');
       } 
    public function btmHome()
       {
         return view('btm.home');
       }   
    public function bdrHome()
       {
         return view('bdr.home');
       }   
    public function htdHome()
       {
         return view('hetauda.home');
       }  
    public function gjrHome()
       {
         return view('gjr.home');
       }  
    public function unitHome()
       {
         return view('unit.home');
       }   
    public function npjHome()
       {
         return view('npj.home');
       }         
    public function adminHome()
       {
        return view('headoffice.home');
       }   
    public function edit($id)
      {
        $officedata = Officedata::find($id);
        return view('officedata.edit',compact('officedata'));
      }
    public function update(Request $request, $id)
    {
      $officedata = Officedata::find($id);
      $this->validate($request, array(
      'fiscal_year' => 'required',
      'month' => 'required',
      'member_dalit_male' => 'required',
      'member_dalit_female' => 'required',
      'member_janajati_male' => 'required',
      'member_janajati_female' => 'required',
      'member_other_male' => 'required',
      'member_other_female' => 'required',
      'total_member' => 'required',
      'loaned_male_number' =>'required',
      'loaned_female_number' => 'required',
      'total_loaned_people' => 'required',
      'laganima_raheko_rakam' => 'required',
      'lagani' => 'required',
      'sewa_asuli' => 'required',
      'byaj_asuli' => 'required',
      'vaka_nageko_rakam' => 'required',
      'paunu_parne__baki_byaj' => 'required',
      'antarik_source_share_pungi' => 'required',
      'antarik_source_jageda_kosh' => 'required',
      'antarik_source_samuha_bachat' => 'required',
      'antarik_source_other' => 'required',
      'total_antarik_source_pungi' => 'required',
      'skbbl_bata_lieko_loan' => 'required',
      'skbbl_lai_bujaeko_loan' => 'required',
      'skbbl_lai_tirna_baki_loan' => 'required',
      'aru_bata_lieko_loan' => 'required',
      'aru_lai_bujaeko_loan' => 'required',
      'aru_lai_tirna_baki_loan' => 'required',
      'total_external_loan' => 'required',
      'total_income' => 'required',
      'total_expense' => 'required',
      'profit_loss' => 'required',
      'other_income' => 'required',
      'byaj_income' => 'required',
      'byaj_expense' => 'required',
      'karmachari_parsasanik_expense' => 'required',
      'jokhim_kosh_expense' => 'required'
      ));
      $officedataupdate = Officedata::where('id', $id)
         ->update([
          'fiscal_year'=> $request->fiscal_year,
          'month' => $request->month,
          'member_dalit_male' => $request->member_dalit_male,
          'member_dalit_female' => $request->member_dalit_female,
          'member_janajati_male' => $request->member_janajati_male,
          'member_janajati_female' => $request->member_janajati_female,
          'member_other_male' => $request->member_other_male,
          'member_other_female' => $request->member_other_female,
          'total_member' => $request->total_member,
          'loaned_male_number' => $request->loaned_male_number,
          'loaned_female_number' => $request->loaned_female_number,
          'total_loaned_people' => $request->total_loaned_people,
          'laganima_raheko_rakam' => $request->laganima_raheko_rakam,
          'lagani' => $request->lagani,
          'sewa_asuli' => $request->sewa_asuli,
          'byaj_asuli' => $request->byaj_asuli,
          'vaka_nageko_rakam' => $request->vaka_nageko_rakam,
          'paunu_parne__baki_byaj' => $request->paunu_parne__baki_byaj,
          'antarik_source_share_pungi' => $request->antarik_source_share_pungi,
          'antarik_source_jageda_kosh' => $request->antarik_source_jageda_kosh,
          'antarik_source_samuha_bachat' => $request->antarik_source_samuha_bachat,
          'antarik_source_other' => $request->antarik_source_other,
          'total_antarik_source_pungi' => $request->total_antarik_source_pungi,
          'skbbl_bata_lieko_loan' => $request->skbbl_bata_lieko_loan,
          'skbbl_lai_bujaeko_loan' => $request->skbbl_lai_bujaeko_loan,
          'skbbl_lai_tirna_baki_loan' => $request->skbbl_lai_tirna_baki_loan,
          'aru_bata_lieko_loan' => $request->aru_bata_lieko_loan,
          'aru_lai_bujaeko_loan' => $request->aru_lai_bujaeko_loan,
          'aru_lai_tirna_baki_loan' => $request->aru_lai_tirna_baki_loan,
          'total_external_loan' => $request->total_external_loan,
          'byaj_income' => $request->byaj_income,
          'other_income' => $request->other_income,
          'total_income' => $request->total_income,
          'byaj_expense' => $request->byaj_expense,
          'karmachari_parsasanik_expense' => $request->karmachari_parsasanik_expense,
          'jokhim_kosh_expense' => $request->jokhim_kosh_expense,
          'total_expense' => $request->total_expense,
          'profit_loss' => $request->profit_loss
      ]);  
         $user = Auth::user();
         if($user->role == 2)
           {
              return redirect()->route('admin-alldata');
           } 
          if($user->role == 1){
                return redirect()->route('butwal-alldata');
              }  
          if($user->role == 3){
                return redirect()->route('pokhara-alldata');
              } 
          if($user->role == 4){
                return redirect()->route('hetauda-alldata');
              } 
          if($user->role == 5){
                return redirect()->route('gajuri-alldata');
              } 
          if($user->role == 6){
                return redirect()->route('bardibas-alldata');
              }     
          if($user->role == 7){
                return redirect()->route('itahari-alldata');
              } 

          if($user->role == 8){
                return redirect()->route('birtamod-alldata');
              } 
          if($user->role == 9){
                return redirect()->route('surkhet-alldata');
              } 
          if($user->role == 10){
                return redirect()->route('attariya-alldata');
              }  
          if($user->role == 11){
                return redirect()->route('nepalginj-alldata');
              } 
          if($user->role == 12){
                return redirect()->route('unit-alldata');
              }                                  

    }  
    public function destroy($id)
    {
      $officedata = Officedata::find($id);
      $officedata->delete();
       $user = Auth::user();
          if($user->role == 2)
           {
              return redirect()->route('admin-alldata');
          }
          if($user->role == 1){
                return redirect()->route('butwal-alldata');
              }  
          if($user->role == 3){
                return redirect()->route('pokhara-alldata');
              } 
          if($user->role == 4){
                return redirect()->route('hetauda-alldata');
              } 
          if($user->role == 5){
                return redirect()->route('gajuri-alldata');
              } 
          if($user->role == 6){
                return redirect()->route('bardibas-alldata');
              }     
          if($user->role == 7){
                return redirect()->route('itahari-alldata');
              } 

          if($user->role == 8){
                return redirect()->route('birtamod-alldata');
              } 
          if($user->role == 9){
                return redirect()->route('surkhet-alldata');
              } 
          if($user->role == 10){
                return redirect()->route('attariya-alldata');
              }  
          if($user->role == 11){
                return redirect()->route('nepalginj-alldata');
              } 
          if($user->role == 12){
                return redirect()->route('unit-alldata');
              }                                  
         
     
    }
    
    public function butwalData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',2)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',2)->with('office')->with('area')->get();
      return view('butwal.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
     public function butwalNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',2)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('butwal.notreceived',compact('notreceived'));

    }
    public function attariyaData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',1)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',1)->with('office')->with('area')->get();
      return view('attariya.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
     public function attariyaNotreceived()
    {
        $this->fdate = new Officedata();
      
        $notreceived = Office::where('area_office_id',1)->wheredoesnthave('officedatas', function($q)
        {
           $fyear = $this->fdate->getYear();
           $fmonth = $this->fdate->getMonth();
           $q->where('month', $fmonth)->where('fiscal_year',$fyear);
        })->get();   
        return view('attariya.notreceived',compact('notreceived'));
    }
     public function htdData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',3)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',3)->with('office')->with('area')->get();
      return view('hetauda.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
     public function htdNotreceived()
    {
        $this->fdate = new Officedata();
      
        $notreceived = Office::where('area_office_id',3)->wheredoesnthave('officedatas', function($q)
        {
           $fyear = $this->fdate->getYear();
           $fmonth = $this->fdate->getMonth();
           $q->where('month', $fmonth)->where('fiscal_year',$fyear);
        })->get();   
        return view('hetauda.notreceived',compact('notreceived'));
    }
    public function btmData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',4)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',4)->with('office')->with('area')->get();
      return view('btm.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
     public function btmNotreceived()
    {
        $this->fdate = new Officedata();
      
        $notreceived = Office::where('area_office_id',4)->wheredoesnthave('officedatas', function($q)
        {
           $fyear = $this->fdate->getYear();
           $fmonth = $this->fdate->getMonth();
           $q->where('month', $fmonth)->where('fiscal_year',$fyear);
        })->get();   
        return view('btm.notreceived',compact('notreceived'));
    }
    public function itrData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',5)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',5)->with('office')->with('area')->get();
      return view('itr.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
     public function itrNotreceived()
    {
        $this->fdate = new Officedata();
      
        $notreceived = Office::where('area_office_id',5)->wheredoesnthave('officedatas', function($q)
        {
           $fyear = $this->fdate->getYear();
           $fmonth = $this->fdate->getMonth();
           $q->where('month', $fmonth)->where('fiscal_year',$fyear);
        })->get();   
        return view('itr.notreceived',compact('notreceived'));
    }

    public function pkrData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',6)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',6)->with('office')->with('area')->get();
      return view('pokhara.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
    public function pkrNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',6)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('pokhara.notreceived',compact('notreceived'));
    }
    public function bdrData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',7)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',7)->with('office')->with('area')->get();
      return view('bdr.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
    public function bdrNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',7)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('bdr.notreceived',compact('notreceived'));
    }
    public function gjrData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',8)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',8)->with('office')->with('area')->get();
      return view('gjr.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
    public function gjrNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',8)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('gjr.notreceived',compact('notreceived'));
    }
    public function npjData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',9)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',9)->with('office')->with('area')->get();
      return view('npj.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
    public function npjNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',9)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('npj.notreceived',compact('notreceived'));
    }
    public function jkrData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',10)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',10)->with('office')->with('area')->get();
      return view('jkr.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
    public function jkrNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',10)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('jkr.notreceived',compact('notreceived'));
    }
    public function unitData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $office = Office::where('area_office_id',11)->get();
      $data = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->where('area_office_id',11)->with('office')->with('area')->get();
      return view('unit.activedata',compact('data','office','finaldate'));
      //return $finaldate;
    }
    public function unitNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::where('area_office_id',11)->wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('unit.notreceived',compact('notreceived'));
    }
    public function adminData()
    {
      $this->fdate = new Officedata();
      $fyear = $this->fdate->getYear();
      $fmonth = $this->fdate->getMonth();
      $finaldate = $fyear."-".$fmonth;
      $adminoffice = Office::get();
      $admindata = Officedata::where('fiscal_year',$fyear)->where('month',$fmonth)->with('office')->with('area')->get();
      return view('headoffice.activedata',compact('admindata',
        'adminoffice','finaldate'));
      //return $finaldate;
    }
    public function adminNotreceived()
    {
      $this->fdate = new Officedata();
      
      $notreceived = Office::wheredoesnthave('officedatas', function($q)
      {
         $fyear = $this->fdate->getYear();
         $fmonth = $this->fdate->getMonth();
         $q->where('month', $fmonth)->where('fiscal_year',$fyear);
      })->get();   
      return view('headoffice.notreceived',compact('notreceived'));

    }
   













    public function index()
    {
         $edate = date('Y-m-d');
         $arr = explode("-", $edate);
         $eyear = $arr['0'];
         $emonth = $arr['1'];
         $edays = $arr['2'];
         $tempdays = $edays + 16;
         $tempmonth = $emonth + 8;
         $tempyear = $eyear + 56;
         if($tempdays > 30)
               {
                 $tempdays = $tempdays - 30;
                 $tempmonth = $tempmonth + 1;
               }

          if($tempmonth > 12)
               {
                 $tempyear = $tempyear + 1;
                 $tempmonth = $tempmonth - 12;
               }
          $nyear = $tempyear;
          $nmonth = $tempmonth - 1;
          //$nmonth = 4;
          $ndays = $tempdays;
          if($nmonth <= 3 && $nmonth >= 1)
          {
             $syear = str_split($nyear,2);
             $secondyear  = $syear[1];
            //$secondyear = explode($nyear,-2);
            //  $stringyear = implode($secondyear);
            $arthikyear = ($nyear - 1).'/'.$secondyear;
          }
          elseif($nmonth >= 4 && $nmonth <= 12){
            $syear = str_split($nyear,2);
            $secondyear = $syear[1];
            //$secondyear = explode($nyear,2);
            //$stringyear = implode($secondyear);
            $arthikyear = $nyear.'/'.($secondyear + 1);
          }
           else
           {
             echo "error in arthik error";
           }
          $finaldate = $arthikyear."-".$nmonth;
          $area = Area::get();
          $office = Office::get();
          $officedata = Officedata::where('fiscal_year',$arthikyear)->where('month',$nmonth)->with('office')->with('area')->get();
          return view('officedata.index',compact('officedata','area','office','finaldate'));

        //return view('officedata.index');
    }

    public function create()
    {
        return view('officedata.index');
    }

    public function exportreceived()
    {
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
        if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

         if($tempmonth > 12)
              {
                $tempyear = $tempyear + 1;
                $tempmonth = $tempmonth - 12;
              }
       $nyear = $tempyear;
       $nmonth = $tempmonth - 1;
       $ndays = $tempdays;
       if($nmonth <= 3 && $nmonth >= 1)
        {
           $syear = str_split($nyear,2);
           $secondyear  = $syear[1];
          //$secondyear = explode($nyear,-2);
          //  $stringyear = implode($secondyear);
          $arthikyear = ($nyear - 1).'-'.$secondyear;
        }
        elseif($nmonth >= 4 && $nmonth <= 12){
          $syear = str_split($nyear,2);
          $secondyear = $syear[1];
          //$secondyear = explode($nyear,2);
          //$stringyear = implode($secondyear);
          $arthikyear = $nyear.'-'.($secondyear + 1);
        }
         else
         {
           echo "error in arthik error";
         }
        $filename = $arthikyear."-".$nmonth;
        return Excel::download(new ReceivedExport,$filename.'.xlsx');
    }

    public function databyArea($id)
    {
        $area = Area::get();
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
        if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

         if($tempmonth > 12)
              {
                $tempyear = $tempyear + 1;
                $tempmonth = $tempmonth - 12;
              }
       $nyear = $tempyear;
       $nmonth = $tempmonth - 1;
       $ndays = $tempdays;
       if($nmonth <= 3 && $nmonth >= 1)
       {
          $syear = str_split($nyear,2);
          $secondyear  = $syear[1];
         //$secondyear = explode($nyear,-2);
         //  $stringyear = implode($secondyear);
         $arthikyear = ($nyear - 1).'/'.$secondyear;
       }
       elseif($nmonth >= 4 && $nmonth <= 12){
         $syear = str_split($nyear,2);
         $secondyear = $syear[1];
         //$secondyear = explode($nyear,2);
         //$stringyear = implode($secondyear);
         $arthikyear = $nyear.'/'.($secondyear + 1);
       }
        else
        {
          echo "error in arthik error";
        }
       $finaldate = $arthikyear."-".$nmonth;
        $office = Office::where('area_office_id',$id)->get();

        $officedata = Officedata::where('area_office_id',$id)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->get();
        $areadata = Office::where('area_office_id',$id)->pluck('area_office_id')->first();

       return view('areas.index',compact('officedata','area','areadata','office','finaldate'));

    }

    public function exportbyarea($areadata)
    {
      // return (new AreadataExport($areadata))->download('area.xlsx');
      $edate = date('Y-m-d');
      $arr = explode("-", $edate);
      $eyear = $arr['0'];
      $emonth = $arr['1'];
      $edays = $arr['2'];
      $tempdays = $edays + 16;
      $tempmonth = $emonth + 8;
      $tempyear = $eyear + 56;
      if($tempdays > 30)
            {
              $tempdays = $tempdays - 30;
              $tempmonth = $tempmonth + 1;
            }

       if($tempmonth > 12)
            {
              $tempyear = $tempyear + 1;
              $tempmonth = $tempmonth - 12;
            }
     $nyear = $tempyear;
     $nmonth = $tempmonth - 1;
     $ndays = $tempdays;
     if($nmonth <= 3 && $nmonth >= 1)
        {
           $syear = str_split($nyear,2);
           $secondyear  = $syear[1];
          //$secondyear = explode($nyear,-2);
          //  $stringyear = implode($secondyear);
          $arthikyear = ($nyear - 1).'-'.$secondyear;
        }
        elseif($nmonth >= 4 && $nmonth <= 12){
          $syear = str_split($nyear,2);
          $secondyear = $syear[1];
          //$secondyear = explode($nyear,2);
          //$stringyear = implode($secondyear);
          $arthikyear = $nyear.'-'.($secondyear + 1);
        }
         else
         {
           echo "error in arthik error";
         }
        $filename = $arthikyear."-".$nmonth;
       return Excel::download(new AreadataExport($areadata), $filename."-".'.xlsx');

    }

    public function received()
    {
        $area = Area::get();
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
        if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

         if($tempmonth > 12)
              {
                $tempyear = $tempyear + 1;
                $tempmonth = $tempmonth - 12;
              }
       $nyear = $tempyear;
       $nmonth = $tempmonth - 1;
       $ndays = $tempdays;
       if($nmonth <= 3 && $nmonth >= 1)
       {
          $syear = str_split($nyear,2);
          $secondyear  = $syear[1];
         //$secondyear = explode($nyear,-2);
         //  $stringyear = implode($secondyear);
         $arthikyear = ($nyear - 1).'/'.$secondyear;
       }
       elseif($nmonth >= 4 && $nmonth <= 12){
         $syear = str_split($nyear,2);
         $secondyear = $syear[1];
         //$secondyear = explode($nyear,2);
         //$stringyear = implode($secondyear);
         $arthikyear = $nyear.'/'.($secondyear + 1);
       }
        else
        {
          echo "error in arthik error";
        }
       $finaldate = $arthikyear."-".$nmonth;
        //$finaldate =  $nyear."-".$nmonth;
        $received = Officedata::where('fiscal_year',$arthikyear)->where('month',$nmonth)->get();
        return view('officedata.received',compact('received','area','nyear','nmonth','finaldate'));
    }

    public function notreceived()
    {
        $area = Area::get();
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
          if($tempdays > 30)
                {
                  $tempdays = $tempdays - 30;
                  $tempmonth = $tempmonth + 1;
                }

           if($tempmonth > 12)
                {
                  $tempyear = $tempyear + 1;
                  $tempmonth = $tempmonth - 12;
                }
         $nyear = $tempyear;
         $nmonth = $tempmonth;
         $ndays = $tempdays;
          //$finaldate =  $nyear."-".$nmonth;
         $notreceived = Office::wheredoesnthave('officedatas', function($q)
         {
          $edate = date('Y-m-d');
          $arr = explode("-", $edate);
          $eyear = $arr['0'];
          $emonth = $arr['1'];
          $edays = $arr['2'];
          $tempdays = $edays + 16;
          $tempmonth = $emonth + 8;
          $tempyear = $eyear + 56;
          if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

           if($tempmonth > 12)
                {
                  $tempyear = $tempyear + 1;
                  $tempmonth = $tempmonth - 12;
                }
         $nyear = $tempyear;
         $nmonth = $tempmonth - 1;
         $ndays = $tempdays;
         if($nmonth <= 3 && $nmonth >= 1)
         {
            $syear = str_split($nyear,2);
            $secondyear  = $syear[1];
           //$secondyear = explode($nyear,-2);
           //  $stringyear = implode($secondyear);
           $arthikyear = ($nyear - 1).'/'.$secondyear;
         }
         elseif($nmonth >= 4 && $nmonth <= 12){
           $syear = str_split($nyear,2);
           $secondyear = $syear[1];
           //$secondyear = explode($nyear,2);
           //$stringyear = implode($secondyear);
           $arthikyear = $nyear.'/'.($secondyear + 1);
         }
          else
          {
            echo "error in arthik error";
          }
          //$finaldate =  $nyear."-".$nmonth;
         $q->where('month', $nmonth)->where('fiscal_year',$arthikyear);
         }
         )->get();
          return view('officedata.notreceived',compact('notreceived','area','nmonth'));
    }

    public function areareceived($areadata)
    {
        $area = Area::get();
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
        if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

         if($tempmonth > 12)
              {
                $tempyear = $tempyear + 1;
                $tempmonth = $tempmonth - 12;
              }
       $nyear = $tempyear;
       $nmonth = $tempmonth - 1;
       $ndays = $tempdays;
       if($nmonth <= 3 && $nmonth >= 1)
       {
          $syear = str_split($nyear,2);
          $secondyear  = $syear[1];
         //$secondyear = explode($nyear,-2);
         //  $stringyear = implode($secondyear);
         $arthikyear = ($nyear - 1).'/'.$secondyear;
       }
       elseif($nmonth >= 4 && $nmonth <= 12){
         $syear = str_split($nyear,2);
         $secondyear = $syear[1];
         //$secondyear = explode($nyear,2);
         //$stringyear = implode($secondyear);
         $arthikyear = $nyear.'/'.($secondyear + 1);
       }
        else
        {
          echo "error in arthik error";
        }
       $finaldate = $arthikyear."-".$nmonth;
        $officedata = Officedata::where('area_office_id',$areadata)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->get();

        return view('areas.areareceived',compact('area','officedata'));

    }
    public function areanotreceived($areadata)
    {
        $area = Area::get();
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
        if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

         if($tempmonth > 12)
              {
                $tempyear = $tempyear + 1;
                $tempmonth = $tempmonth - 12;
              }
       $nyear = $tempyear;
       $nmonth = $tempmonth - 1;
       $ndays = $tempdays;
         //$finaldate =  $nyear."-".$nmonth;
          $notreceived = Office::where('area_office_id',$areadata)->wheredoesnthave('officedatas', function($q) use($areadata)
            {

            $edate = date('Y-m-d');
            $arr = explode("-", $edate);
            $eyear = $arr['0'];
            $emonth = $arr['1'];
            $edays = $arr['2'];
            $tempdays = $edays + 16;
            $tempmonth = $emonth + 8;
            $tempyear = $eyear + 56;
            if($tempdays > 30)
                  {
                    $tempdays = $tempdays - 30;
                    $tempmonth = $tempmonth + 1;
                  }

             if($tempmonth > 12)
                  {
                    $tempyear = $tempyear + 1;
                    $tempmonth = $tempmonth - 12;
                  }
           $nyear = $tempyear;
           $nmonth = $tempmonth - 1;
           $ndays = $tempdays;
           if($nmonth <= 3 && $nmonth >= 1)
           {
              $syear = str_split($nyear,2);
              $secondyear  = $syear[1];
               //$secondyear = explode($nyear,-2);
               //  $stringyear = implode($secondyear);
               $arthikyear = ($nyear - 1).'/'.$secondyear;
            }
           elseif($nmonth >= 4 && $nmonth <= 12){
             $syear = str_split($nyear,2);
             $secondyear = $syear[1];
             //$secondyear = explode($nyear,2);
             //$stringyear = implode($secondyear);
             $arthikyear = $nyear.'/'.($secondyear + 1);
           }
            else
            {
              echo "error in arthik error";
            }
           $q->where('month', $nmonth)->where('fiscal_year',$arthikyear);
           }
           )->get();
          //$notreceived = Officedata::where('area_office_id',$areadata)->where('status',0)->get();
          return view('areas.areanotreceived',compact('notreceived','area','nyear','nmonth'));

    }



}
?>
