<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\OfficesExport;
use App\Imports\OfficesImport;
use App\Office;
use App\Area;
use App\Officedata;
use Auth;
use APP\User;
use Excel;


class OfficeController extends Controller
{
      public function __construct()
      {
         $this->middleware('auth');
      }

      
      public function edit($id)
      {
          $office = office::with('user')->find($id);
          return view('offices.edit',compact('office'));
      }
      public function update(Request $request, $id)
      {
        $office = Office::with('user')->find($id);
        
        $this->validate($request, array(
        'coperative_name' => 'required',
        'pan_no' => 'required',
        'province' => 'required',
        'district' => 'required',
        'palika' => 'required',
        'username' => 'required',
        'password' =>'required'
        ));
        $officeupdate = Office::where('id', $id)
        ->update([
            'coperative_name'=> $request->coperative_name,
            'pan_no' => $request->pan_no,
            'province' => $request->province,
            'district' => $request->district,
            'palika' => $request->palika,
            'mobile_no' => $request->mobile_no
        ]);
        $user = $office->user()
          ->update([
            'username'=> $request->username,
            'password'=> $request->password
           ]);
          $user = Auth::user();
          if($user->role == 2){
                return redirect()->route('admin-users');
              }
          if($user->role == 1){
                return redirect()->route('butwal-users');
              }  
          if($user->role == 3){
                return redirect()->route('pokhara-users');
              }   
          if($user->role == 4){
                return redirect()->route('hetauda-users');
              } 
          if($user->role == 5){
                return redirect()->route('gajuri-users');
              } 
          if($user->role == 6){
                return redirect()->route('bardibas-users');
              }     
          if($user->role == 7){
                return redirect()->route('itahari-users');
              } 

          if($user->role == 8){
                return redirect()->route('birtamod-users');
              } 
          if($user->role == 9){
                return redirect()->route('surkhet-users');
              } 
          if($user->role == 10){
                return redirect()->route('attariya-users');
              }  
          if($user->role == 11){
                return redirect()->route('nepalginj-users');
              } 
          if($user->role == 12){
                return redirect()->route('unit-users');
              }                                  
         
          
      }
       public function atrUsers()
       {
         $user = Office::where('area_office_id',1)->get();
         return view('attariya.office',compact('user'));
       }
       public function butwalUsers()
       {
         $user = Office::where('area_office_id',2)->get();
         return view('butwal.office',compact('user'));
       }
       public function htdUsers()
       {
         $user = Office::where('area_office_id',3)->get();
         return view('hetauda.office',compact('user'));
       }
       public function btmUsers()
       {
         $user = Office::where('area_office_id',4)->get();
         return view('btm.office',compact('user'));
       }
       public function itrUsers()
       {
         $user = Office::where('area_office_id',5)->get();
         return view('itr.office',compact('user'));
       }
      public function pokharaUsers()
      {
        $user = Office::where('area_office_id',6)->get();
        return view('pokhara.office',compact('user'));
      }
      public function bdrUsers()
       {
         $user = Office::where('area_office_id',7)->get();
         return view('bdr.office',compact('user'));
       }
       public function gjrUsers()
       {
         $user = Office::where('area_office_id',8)->get();
         return view('gjr.office',compact('user'));
       }
       public function npjUsers()
       {
         $user = Office::where('area_office_id',9)->get();
         return view('npj.office',compact('user'));
       }
       public function jkrUsers()
       {
         $user = Office::where('area_office_id',10)->get();
         return view('jkr.office',compact('user'));
       }
       public function unitUsers()
       {
         $user = Office::where('area_office_id',11)->get();
         return view('unit.office',compact('user'));
       }
      public function adminUsers()
      {
        $adminuser = Office::get();
        return view('headoffice.office',compact('adminuser'));
      }    

    public function export()
    {


        return Excel::download(new OfficesExport, 'exportcoperativeinfo.xlsx');
    }


    public function create()
    {
        return view('offices.create');
    }

    public function store(Request $request)
    {
        $office = new Office;
        $request->validate([
			   'username' => 'required|max:255',
            'password' => 'required'

        ]);

          $office->create(['coperative_name' => $request->coperative_name,'username' => $request->username,
                         'password' => $request->password,'mobile_no' => $request->mobile_no,
                         'email' => $request->email,'coperative_type'=> $request->coperative_type,
                         'pan_no' => $request->pan_no,'area_office' => $request->area_office,
                         'province' => $request->province, 'district' => $request->district,
                         'palika' => $request->palika,'palika_type' => $request->palika_type,
                         'ward_no' => $request->ward_no,'status' => $request->status]);
        return "success";

    }
    public function importData()
    {
      Excel::import(new OfficesImport,request()->file('file'));
      return back();
    }

    public function destroy($id)
    {
        //
    }
}
