<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Officedata;
use App\Office;
use App\Area;
use App\User;
use Excel;
use Response;
use DataTables;

class BackupDataController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');

    }
     public function btlallData()
    {
      $data = Officedata::where('area_office_id',2)->get();
      return view('butwal.alldata',compact('data'));
    }
    public function atrallData()
    {
      $data = Officedata::where('area_office_id',1)->get();
      return view('attariya.alldata',compact('data'));
    }
    public function htdallData()
    {
      $data = Officedata::where('area_office_id',3)->get();
      return view('htd.alldata',compact('data'));
    }
    public function btmallData()
    {
      $data = Officedata::where('area_office_id',4)->get();
      return view('btm.alldata',compact('data'));
    }
    public function itrallData()
    {
      $data = Officedata::where('area_office_id',5)->get();
      return view('itr.alldata',compact('data'));
    }
    public function pkrallData()
    {
      $data = Officedata::where('area_office_id',6)->get();
      return view('pokhara.alldata',compact('data'));
    }
    public function bdrallData()
    {
      $data = Officedata::where('area_office_id',7)->get();
      return view('bdr.alldata',compact('data'));
    }
    public function gjrallData()
    {
      $data = Officedata::where('area_office_id',8)->get();
      return view('gjr.alldata',compact('data'));
    }
    public function npjallData()
    {
      $data = Officedata::where('area_office_id',9)->get();
      return view('npj.alldata',compact('data'));
    }
    public function jkrallData()
    {
      $data = Officedata::where('area_office_id',10)->get();
      return view('jkr.alldata',compact('data'));
    }
    public function unitallData()
    {
      $data = Officedata::where('area_office_id',11)->get();
      return view('unit.alldata',compact('data'));
    }
     
     public function adminallData()
    {
      $admindata = Officedata::get();
      return view('headoffice.alldata',compact('admindata'));
    }

    public function getdata(Request $request)
    {
     // return datatables()->of(User::all())->toJson();
      return datatables()->of(Officedata::query()->with('office', 'user','area'))
      ->addColumn('officename', function($row){
          return $row->office->coperative_name;
      })
      ->addColumn('area', function($row){
          return $row->area->name;
      })
      ->addColumn('district', function($row){
        return $row->office->district;
      })

      ->addColumn('action', function ($row) {
      return '<a href="" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i>Show all</a>';
      })

      ->make(true);

    }
      
    public function index()
    {
      $area = Area::get();
      $officedata = Officedata::get();
      return view('backup.index',compact('area','officedata'));
    }

    public function allData()
    {
      $area = Area::get();
      $office = Office::get();
      $officedata = Officedata::with('office')->with('area')->get();

      return view('backup.alldata',compact('officedata','area','office'));
    }
}
