<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use App\Area;
use App\Office;
use App\Officedata;
use App\User;

class HomeController extends Controller
{
    
  public function __construct()
    {
        $this->middleware('auth');
    }


  public function index()
    {
         $edate = date('Y-m-d');
         $arr = explode("-", $edate);
         $eyear = $arr['0'];
         $emonth = $arr['1'];
         $edays = $arr['2'];
         $tempdays = $edays + 16;
         $tempmonth = $emonth + 8;
         $tempyear = $eyear + 56;
         if($tempdays > 30)
               {
                 $tempdays = $tempdays - 30;
                 $tempmonth = $tempmonth + 1;
               }

          if($tempmonth > 12)
               {
                 $tempyear = $tempyear + 1;
                 $tempmonth = $tempmonth - 12;
               }
            $nyear = $tempyear;
            $nmonth = $tempmonth - 1;
            $ndays = $tempdays;
            if($nmonth <= 3 && $nmonth >= 1)
            {
               $syear = str_split($nyear,2);
               $secondyear  = $syear[1];
              //$secondyear = explode($nyear,-2);
              //  $stringyear = implode($secondyear);
              $arthikyear = ($nyear - 1).'/'.$secondyear;
            }
            elseif($nmonth >= 4 && $nmonth <= 12){
              $syear = str_split($nyear,2);
              $secondyear = $syear[1];
              //$secondyear = explode($nyear,2);
              //$stringyear = implode($secondyear);
              $arthikyear = $nyear.'/'.($secondyear + 1);
            }
             else
             {
               echo "error in arthik error";
             }
            $finaldate = $arthikyear."-".$nmonth;
            //$finaldate =  $nyear."-".$nmonth;
            $area = Area::get();
            $user = User::where('role',2)->get();
            $office = Office::get();
            $officedata = Officedata::where('fiscal_year',$arthikyear)->where('month',$nmonth)->get();
            $attariyaoffice = Office::where('area_office_id',1)->get();
            $attariyadata = Officedata::where('area_office_id',1)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $butwaloffice = Office::where('area_office_id',2)->get();
            $butwaldata = Officedata::where('area_office_id',2)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $hetaudaoffice = Office::where('area_office_id',3)->get();
            $hetaudadata = Officedata::where('area_office_id',3)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $birtamodoffice = Office::where('area_office_id',4)->get();
            $birtamoddata =  Officedata::where('area_office_id',4)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $itaharioffice = Office::where('area_office_id',5)->get();
            $itaharidata = Officedata::where('area_office_id',5)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $pokharaoffice = Office::where('area_office_id',6)->get();
            $pokharadata = Officedata::where('area_office_id',6)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $birendranagaroffice = Office::where('area_office_id',7)->get();
            $birendranagardata = Officedata::where('area_office_id',7)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $gajurioffice = Office::where('area_office_id',8)->get();
            $gajuridata = Officedata::where('area_office_id',8)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $nepalganjoffice = Office::where('area_office_id',9)->get();
            $nepalganjdata = Officedata::where('area_office_id',9)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $janakpuroffice = Office::where('area_office_id',10)->get();
            $janakpurdata = Officedata::where('area_office_id',10)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            $unitoffice = Office::where('area_office_id',11)->get();
            $unitdata = Officedata::where('area_office_id',11)->where('fiscal_year',$arthikyear)->where('month', $nmonth)->where('status',1)->get();
            return view('home',compact('user','area','office','officedata','attariyaoffice','attariyadata','butwaloffice','butwaldata',
            'hetaudaoffice','hetaudadata','birtamodoffice','birtamoddata','itaharioffice','itaharidata','pokharaoffice','pokharadata',
            'birendranagaroffice','birendranagardata','gajurioffice','gajuridata','nepalganjoffice','nepalganjdata','janakpuroffice',
            'janakpurdata','unitoffice','unitdata','nyear','nmonth'));
          
    }

  public function showChangePasswordForm()
   {
         $area = Area::get();
        return view('auth.changepassword',compact('area'));
    }
  public function changePassword(Request $request)
   {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
        // The passwords matches
        return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
        //Current password and new password are same
        return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
        'current-password' => 'required',
        'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
   }

}
