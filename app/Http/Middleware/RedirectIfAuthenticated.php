<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    
   /* public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);
    } */
    public function handle($request, Closure $next, $guard = null)
     {
          if (Auth::guard($guard)->check()) 
          {
            $role = Auth::user()->role; 

                switch ($role) 
                {
                  case '1':
                     return redirect('/butwal-home');
                     break;
                  case '2':
                     return redirect('/admin-home');
                     break;
                  case '3':
                     return redirect('/pokhara-home');
                     break;
                  case '4':
                    return redirect('/hetauda-home');
                    break; 
                  case '5':
                    return redirect('/gajuri-home');
                    break;
                  case '6':
                    return redirect('/bardibas-home');
                    break;  
                  case '7':
                    return redirect('/itahari-home');
                    break;    
                  case '8':
                    return redirect('/birtamod-home');
                    break;   
                  case '9':
                    return redirect('/surkhet-home');
                    break; 
                  case '10':
                    return redirect('/attariya-home');
                    break;
                  case '11':
                    return redirect('/nepalgunj-home');
                    break; 
                  case '12':
                    return redirect('/unit-home');
                    break;          
          

                  
                }
          }
      return $next($request);
    }
}
