<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Exceptions\PermissionDenied;


class CheckPermission
{
    
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

  

   public function handle($request, Closure $next, string $role)
    {
           $user = Auth::user();

            if($user->role == $role){
              return $next($request);
            }

           return response('Not authorized');
            
    }
    
}
