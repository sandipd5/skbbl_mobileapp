<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
  protected $table = 'area_offices';

  protected $fillable = ['name','code'];

  public function office()
  {
    return $this->hasMany('App\Office');
  }

  public function officedata()
  {
    return $this->hasMany('App\Officedata');
  }


}
