<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officedata extends Model
{
    protected $table = 'office_data';

    protected $fillable =['fiscal_year','month','member_dalit_male','member_dalit_female'
    ,'member_janajati_male','member_janajati_female','member_other_male','member_other_female','total_member'
     ,'loaned_male_number','loaned_female_number','total_loaned_people',
       'laganima_raheko_rakam','lagani','sewa_asuli','byaj_asuli','vaka_nageko_rakam',
       'antarik_source_share_pungi','antarik_source_jageda_kosh','antarik_source_samuha_bachat',
         'antarik_source_other','total_antarik_source_pungi','skbbl_bata_lieko_loan','skbbl_lai_bujaeko_loan',
         'skbbl_lai_tirna_baki_loan','aru_bata_lieko_loan','aru_lai_bujaeko_loan','aru_lai_tirna_baki_loan',
         'total_external_loan','total_income','total_expense','profit_loss','paunu_parne__baki_byaj','byaj_income','other_income','byaj_expense','karmachari_parsasanik_expense','jokhim_kosh_expense','office_id','user_id','area_office_id','status'];



    public function user()
         {
             return $this->belongsTo('App\User', 'user_id','id');

         }

    public function office()
         {
             return $this->belongsTo('App\Office','office_id','id');
         }
    public function area()
      {
        return $this->belongsTo('App\Area','area_office_id','id');
      }
    public function getYear()
    {
         $edate = date('Y-m-d');
         $arr = explode("-", $edate);
         $eyear = $arr['0'];
         $emonth = $arr['1'];
         $edays = $arr['2'];
         $tempdays = $edays + 16;
         $tempmonth = $emonth + 8;
         $tempyear = $eyear + 56;
         if($tempdays > 30)
               {
                 $tempdays = $tempdays - 30;
                 $tempmonth = $tempmonth + 1;
               }

          if($tempmonth > 12)
               {
                 $tempyear = $tempyear + 1;
                 $tempmonth = $tempmonth - 12;
               }
          $nyear = $tempyear;
          $nmonth = $tempmonth - 1;
          $ndays = $tempdays;
          if($nmonth <= 3 && $nmonth >= 1)
          {
             $syear = str_split($nyear,2);
             $secondyear  = $syear[1];
             $arthikyear = ($nyear - 1).'/'.$secondyear;
          }
          elseif($nmonth >= 4 && $nmonth <= 12){
            $syear = str_split($nyear,2);
            $secondyear = $syear[1];
            $arthikyear = $nyear.'/'.($secondyear + 1);
          }
           else
           {
             return "Error in fiscal error";
           }
           return $arthikyear;
    }

     public function getMonth()
    {
         $edate = date('Y-m-d');
         $arr = explode("-", $edate);
         $eyear = $arr['0'];
         $emonth = $arr['1'];
         $edays = $arr['2'];
         $tempdays = $edays + 16;
         $tempmonth = $emonth + 8;
         $tempyear = $eyear + 56;
         if($tempdays > 30)
               {
                 $tempdays = $tempdays - 30;
                 $tempmonth = $tempmonth + 1;
               }

          if($tempmonth > 12)
               {
                 $tempyear = $tempyear + 1;
                 $tempmonth = $tempmonth - 12;
               }
          $nmonth = $tempmonth - 1;
          return $nmonth;
    }
}
