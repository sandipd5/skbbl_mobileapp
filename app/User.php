<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {


	use HasApiTokens, Notifiable;

	protected $table = 'users';
	protected $fillable = [
		'username', 'email', 'password','active','role','office_id'
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	public function office()
    {
		return $this->belongsTo('App\Office','office_id','id');

    }
	public function officedata()
    {
        return $this->hasMany('App\Officedata');
	}
	public function hasRole($role)
	{
	  if ($this->where(‘role’, $role)->first()) {
	    return true;
	  }
	  return false;
	}

   
}