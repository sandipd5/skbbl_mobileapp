<?php
namespace App\Exports;

use App\Officedata;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;



class AreadataExport implements FromView
{
   

    public function __construct(int $areadata)
    {
        $this->areadata = $areadata;
    }

    

    public function view(): View
    {
        $edate = date('Y-m-d');
        $arr = explode("-", $edate);
        $eyear = $arr['0'];
        $emonth = $arr['1'];
        $edays = $arr['2'];
        $tempdays = $edays + 16;
        $tempmonth = $emonth + 8;
        $tempyear = $eyear + 56;
        if($tempdays > 30)
              {
                $tempdays = $tempdays - 30;
                $tempmonth = $tempmonth + 1;
              }

         if($tempmonth > 12)
              {
                $tempyear = $tempyear + 1;
                $tempmonth = $tempmonth - 12;
              }
       $nyear = $tempyear;
       $nmonth = $tempmonth - 1;
       $ndays = $tempdays;
       if($nmonth <= 3 && $nmonth >= 1)
       {
          $syear = str_split($nyear,2);
          $secondyear  = $syear[1];
         $arthikyear = ($nyear - 1).'/'.$secondyear;
       }
       elseif($nmonth >= 4 && $nmonth <= 12){
         $syear = str_split($nyear,2);
         $secondyear = $syear[1];
         $arthikyear = $nyear.'/'.($secondyear + 1);
       }
        else
        {
          echo "error in arthik error";
        }
       return view('exports.areadata', [
       'officedata' => Officedata::where('area_id', $this->areadata)->where('fiscal_year',$arthikyear)->where('month_id',$nmonth)->where('status',1)->get()
        ]);
    }

    
    
}

?>