<?php
namespace App\Exports;

use App\Office;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OfficesExport  implements FromCollection,WithHeadings
{
    public function collection()
    {
        return Office::all();
    }

    public function headings(): array
    {
        return [
            'id','coperative Name','mobile no','coperative type[1=SFCL,2=others]','pan no',
            'area id','province','district','palika','palika type','ward no','created at','updated at'
        ];
    }
}
?>
