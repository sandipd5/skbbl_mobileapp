<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
  protected $table = 'offices';

  protected $fillable = ['coperative_name','mobile_no','coperative_type','pan_no',
                          'area_office_id','province','district','palika','palika_type','ward_no'];


  public function user()
  {
   return $this->hasOne('App\User');
  }
  public function officedatas()
    {
        return $this->hasMany('App\Officedata');
    }
  public function area()
    {
      return $this->belongsTo('App\Area', 'area_office_id','id');
    }
}
