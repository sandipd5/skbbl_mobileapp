<?php
namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel,WithHeadingRow
{
    public function model(array $row)
    {
        return new User([
                'name' => $row['name'],
                'email'   => $row['email'],
                'password' =>bcrypt(($row['password'])),
                'office_id' =>$row['office_id']
        ]);
    }
}
