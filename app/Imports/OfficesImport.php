<?php
namespace App\Imports;

use App\Office;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class OfficesImport implements ToModel,WithHeadingRow
{
  public function model(array $row)
  {
    return new Office([
      'coperative_name' => $row['coperative_name'],
      'mobile_no' => $row['mobile_no'],
      'coperative_type'=>$row['coperative_type'],
      'pan_no'=>$row['pan_no'],
      'area_office_id'=>$row['area_office_id'],
      'province'=>$row['province'],
      'district'=>$row['district'],
      'palika'=>$row['palika'],
      'palika_type'=>$row['palika_type'],
      'ward_no'=>$row['ward_no'],
    ]);
  }
}
























 ?>
