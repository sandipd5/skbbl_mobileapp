--
-- PostgreSQL database dump
--

-- Dumped from database version 10.8
-- Dumped by pg_dump version 10.8

-- Started on 2019-07-18 16:41:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2919 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 213 (class 1259 OID 16907)
-- Name: area_office; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.area_office (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    code character varying(191) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.area_office OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16905)
-- Name: area_office_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.area_office_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.area_office_id_seq OWNER TO postgres;

--
-- TOC entry 2920 (class 0 OID 0)
-- Dependencies: 212
-- Name: area_office_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.area_office_id_seq OWNED BY public.area_office.id;


--
-- TOC entry 197 (class 1259 OID 16805)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(191) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16803)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2921 (class 0 OID 0)
-- Dependencies: 196
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 202 (class 1259 OID 16840)
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_access_tokens (
    id character varying(100) NOT NULL,
    user_id integer,
    client_id integer NOT NULL,
    name character varying(191),
    scopes text,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_access_tokens OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16832)
-- Name: oauth_auth_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_auth_codes (
    id character varying(100) NOT NULL,
    user_id integer NOT NULL,
    client_id integer NOT NULL,
    scopes text,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_auth_codes OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16857)
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_clients (
    id integer NOT NULL,
    user_id integer,
    name character varying(191) NOT NULL,
    secret character varying(100) NOT NULL,
    redirect text NOT NULL,
    personal_access_client boolean NOT NULL,
    password_client boolean NOT NULL,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_clients OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16855)
-- Name: oauth_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oauth_clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_clients_id_seq OWNER TO postgres;

--
-- TOC entry 2922 (class 0 OID 0)
-- Dependencies: 204
-- Name: oauth_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oauth_clients_id_seq OWNED BY public.oauth_clients.id;


--
-- TOC entry 207 (class 1259 OID 16869)
-- Name: oauth_personal_access_clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_personal_access_clients (
    id integer NOT NULL,
    client_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_personal_access_clients OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16867)
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oauth_personal_access_clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_personal_access_clients_id_seq OWNER TO postgres;

--
-- TOC entry 2923 (class 0 OID 0)
-- Dependencies: 206
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oauth_personal_access_clients_id_seq OWNED BY public.oauth_personal_access_clients.id;


--
-- TOC entry 203 (class 1259 OID 16849)
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_refresh_tokens (
    id character varying(100) NOT NULL,
    access_token_id character varying(100) NOT NULL,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_refresh_tokens OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16894)
-- Name: office_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.office_data (
    id bigint NOT NULL,
    fiscal_year character varying(191) NOT NULL,
    month_id integer NOT NULL,
    member_dalit_male integer,
    member_dalit_female integer,
    member_janajati_male integer,
    member_janajati_female integer,
    member_other_male integer,
    member_other_female integer,
    total_member integer,
    loaned_male_number integer,
    loaned_female_number integer,
    total_loaned_people integer,
    laganima_raheko_rakam numeric(12,2),
    lagani numeric(12,2),
    sewa_asuli numeric(12,2),
    byaj_asuli numeric(12,2),
    vaka_nageko_rakam numeric(12,2),
    antarik_source_share_pungi numeric(12,2),
    antarik_source_jageda_kosh numeric(12,2),
    antarik_source_samuha_bachat numeric(12,2),
    antarik_source_other numeric(12,2),
    total_antarik_source_pungi numeric(12,2),
    skbbl_bata_lieko_loan numeric(12,2),
    skbbl_lai_bujaeko_loan numeric(12,2),
    skbbl_lai_tirna_baki_loan numeric(12,2),
    aru_bata_lieko_loan numeric(12,2),
    aru_lai_bujaeko_loan numeric(12,2),
    aru_lai_tirna_baki_loan numeric(12,2),
    total_external_loan numeric(12,2),
    total_income numeric(12,2),
    total_expense numeric(12,2),
    profit_loss numeric(12,2),
    office_id integer NOT NULL,
    user_id integer NOT NULL,
    area_id integer NOT NULL,
    status character varying(255) DEFAULT '1'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    paunu_parne__baki_byaj numeric(12,2),
    byaj_income numeric(12,2),
    other_income numeric(12,2),
    byaj_expense numeric(12,2),
    karmachari_parsasanik_expense numeric(12,2),
    jokhim_kosh_expense numeric(12,2),
    CONSTRAINT office_data_status_check CHECK (((status)::text = ANY ((ARRAY['1'::character varying, '0'::character varying])::text[])))
);


ALTER TABLE public.office_data OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16892)
-- Name: office_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.office_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.office_data_id_seq OWNER TO postgres;

--
-- TOC entry 2924 (class 0 OID 0)
-- Dependencies: 210
-- Name: office_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.office_data_id_seq OWNED BY public.office_data.id;


--
-- TOC entry 209 (class 1259 OID 16878)
-- Name: offices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.offices (
    id bigint NOT NULL,
    coperative_name character varying(191) NOT NULL,
    mobile_no character varying(191),
    coperative_type character varying(255) NOT NULL,
    pan_no character varying(191) NOT NULL,
    area_id integer NOT NULL,
    province character varying(191) NOT NULL,
    district character varying(191) NOT NULL,
    palika character varying(191) NOT NULL,
    palika_type character varying(191) NOT NULL,
    ward_no integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT offices_coperative_type_check CHECK (((coperative_type)::text = ANY ((ARRAY['1'::character varying, '2'::character varying])::text[])))
);


ALTER TABLE public.offices OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16876)
-- Name: offices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.offices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offices_id_seq OWNER TO postgres;

--
-- TOC entry 2925 (class 0 OID 0)
-- Dependencies: 208
-- Name: offices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.offices_id_seq OWNED BY public.offices.id;


--
-- TOC entry 200 (class 1259 OID 16828)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(191) NOT NULL,
    token character varying(191) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16813)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username character varying(191) NOT NULL,
    email character varying(191),
    email_verified_at timestamp(0) without time zone,
    password character varying(191) NOT NULL,
    role character varying(255) DEFAULT '0'::character varying NOT NULL,
    status character varying(255) DEFAULT '1'::character varying NOT NULL,
    office_id bigint,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT users_role_check CHECK (((role)::text = ANY ((ARRAY['0'::character varying, '1'::character varying, '2'::character varying])::text[]))),
    CONSTRAINT users_status_check CHECK (((status)::text = ANY ((ARRAY['1'::character varying, '0'::character varying])::text[])))
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16811)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2926 (class 0 OID 0)
-- Dependencies: 198
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2741 (class 2604 OID 16910)
-- Name: area_office id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_office ALTER COLUMN id SET DEFAULT nextval('public.area_office_id_seq'::regclass);


--
-- TOC entry 2728 (class 2604 OID 16808)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2734 (class 2604 OID 16860)
-- Name: oauth_clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_clients_id_seq'::regclass);


--
-- TOC entry 2735 (class 2604 OID 16872)
-- Name: oauth_personal_access_clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_personal_access_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_personal_access_clients_id_seq'::regclass);


--
-- TOC entry 2738 (class 2604 OID 16897)
-- Name: office_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.office_data ALTER COLUMN id SET DEFAULT nextval('public.office_data_id_seq'::regclass);


--
-- TOC entry 2736 (class 2604 OID 16881)
-- Name: offices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices ALTER COLUMN id SET DEFAULT nextval('public.offices_id_seq'::regclass);


--
-- TOC entry 2729 (class 2604 OID 16816)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2911 (class 0 OID 16907)
-- Dependencies: 213
-- Data for Name: area_office; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.area_office (id, name, code, created_at, updated_at) FROM stdin;
1	attariya	6	2019-06-03 10:15:41	2019-06-03 10:15:41
2	Butwal	10	2019-06-03 10:15:41	2019-06-03 10:15:41
3	Hetauda	5	2019-06-03 10:15:41	2019-06-03 10:15:41
4	Birtamod	1	2019-06-03 10:15:41	2019-06-03 10:15:41
5	Itahari	8	2019-06-03 10:15:41	2019-06-03 10:15:41
6	pokhara	7	2019-06-03 10:15:41	2019-06-03 10:15:41
7	Birendranagar	2	2019-06-03 10:15:41	2019-06-03 10:15:41
8	Gajuri	9	2019-06-03 10:15:41	2019-06-03 10:15:41
9	Nepalgunj	3	2019-06-03 10:15:41	2019-06-03 10:15:41
10	Janakpur	4	2019-06-03 10:15:41	2019-06-03 10:15:41
11	Unit	11	2019-06-18 10:56:23	2019-06-18 10:56:23
\.


--
-- TOC entry 2895 (class 0 OID 16805)
-- Dependencies: 197
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2016_06_01_000001_create_oauth_auth_codes_table	1
4	2016_06_01_000002_create_oauth_access_tokens_table	1
5	2016_06_01_000003_create_oauth_refresh_tokens_table	1
6	2016_06_01_000004_create_oauth_clients_table	1
7	2016_06_01_000005_create_oauth_personal_access_clients_table	1
8	2019_05_22_151731_create_offices_table	1
9	2019_05_22_163641_create_office_data_table	1
10	2019_05_27_122546_create_area_office_table	1
11	2019_07_18_121150_add_coloumns_to_office_data	2
\.


--
-- TOC entry 2900 (class 0 OID 16840)
-- Dependencies: 202
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_access_tokens (id, user_id, client_id, name, scopes, revoked, created_at, updated_at, expires_at) FROM stdin;
\.


--
-- TOC entry 2899 (class 0 OID 16832)
-- Dependencies: 201
-- Data for Name: oauth_auth_codes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_auth_codes (id, user_id, client_id, scopes, revoked, expires_at) FROM stdin;
\.


--
-- TOC entry 2903 (class 0 OID 16857)
-- Dependencies: 205
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_clients (id, user_id, name, secret, redirect, personal_access_client, password_client, revoked, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2905 (class 0 OID 16869)
-- Dependencies: 207
-- Data for Name: oauth_personal_access_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_personal_access_clients (id, client_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2901 (class 0 OID 16849)
-- Dependencies: 203
-- Data for Name: oauth_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_refresh_tokens (id, access_token_id, revoked, expires_at) FROM stdin;
\.


--
-- TOC entry 2909 (class 0 OID 16894)
-- Dependencies: 211
-- Data for Name: office_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.office_data (id, fiscal_year, month_id, member_dalit_male, member_dalit_female, member_janajati_male, member_janajati_female, member_other_male, member_other_female, total_member, loaned_male_number, loaned_female_number, total_loaned_people, laganima_raheko_rakam, lagani, sewa_asuli, byaj_asuli, vaka_nageko_rakam, antarik_source_share_pungi, antarik_source_jageda_kosh, antarik_source_samuha_bachat, antarik_source_other, total_antarik_source_pungi, skbbl_bata_lieko_loan, skbbl_lai_bujaeko_loan, skbbl_lai_tirna_baki_loan, aru_bata_lieko_loan, aru_lai_bujaeko_loan, aru_lai_tirna_baki_loan, total_external_loan, total_income, total_expense, profit_loss, office_id, user_id, area_id, status, created_at, updated_at, paunu_parne__baki_byaj, byaj_income, other_income, byaj_expense, karmachari_parsasanik_expense, jokhim_kosh_expense) FROM stdin;
723	2075/76	3	1	6	6	5	7	2	10	4	6	15	19043.00	80605.00	81428.00	19704.00	25258.00	3231.00	42578.00	11501.00	18482.00	6140.00	87180.00	75355.00	83020.00	62784.00	36481.00	98924.00	88773.00	12825.00	20776.00	25515.00	658	934	9	1	2019-07-18 15:16:09	2019-07-18 15:16:09	5151.00	98195.00	50624.00	51747.00	45137.00	18354.00
724	2075/76	3	2	8	10	7	6	8	6	3	3	10	90068.00	9886.00	50182.00	75071.00	83330.00	43769.00	93013.00	94250.00	56647.00	62555.00	93244.00	75427.00	29385.00	91860.00	85421.00	57385.00	9714.00	91610.00	67164.00	31539.00	547	1010	5	1	2019-07-18 15:16:09	2019-07-18 15:16:09	96089.00	38785.00	47302.00	56845.00	52503.00	18139.00
725	2075/76	3	1	4	4	6	8	2	10	5	10	19	2264.00	12518.00	47336.00	16799.00	66183.00	89155.00	99520.00	73519.00	45860.00	56103.00	15789.00	18256.00	68014.00	31662.00	10692.00	47807.00	27754.00	66525.00	54955.00	15466.00	310	604	10	1	2019-07-18 15:16:09	2019-07-18 15:16:09	71301.00	47811.00	45717.00	22201.00	79860.00	85924.00
726	2075/76	3	8	10	10	3	3	8	1	6	7	19	28034.00	86919.00	48343.00	1322.00	45535.00	13551.00	47519.00	63263.00	28064.00	39863.00	65117.00	20367.00	11812.00	34637.00	32887.00	94072.00	12219.00	65022.00	46553.00	28255.00	554	690	3	1	2019-07-18 15:16:09	2019-07-18 15:16:09	89847.00	6359.00	59509.00	12770.00	51355.00	69897.00
727	2075/76	3	6	10	2	6	4	7	9	5	4	14	1850.00	95517.00	79840.00	67003.00	31020.00	75993.00	46978.00	91982.00	45824.00	16264.00	96330.00	29052.00	18045.00	31295.00	51100.00	89279.00	17814.00	83988.00	72388.00	78273.00	584	648	7	1	2019-07-18 15:16:09	2019-07-18 15:16:09	75129.00	51311.00	94405.00	4701.00	35465.00	58218.00
728	2075/76	3	3	1	2	9	2	9	7	3	1	16	48581.00	57811.00	64748.00	97473.00	8824.00	66510.00	44405.00	12946.00	51851.00	88004.00	63695.00	75484.00	4992.00	60325.00	86321.00	89119.00	67871.00	66593.00	41488.00	85408.00	477	718	3	1	2019-07-18 15:16:09	2019-07-18 15:16:09	6436.00	63193.00	79369.00	34539.00	27514.00	98411.00
729	2075/76	3	3	3	9	9	10	7	10	2	10	13	39637.00	51233.00	70997.00	79044.00	47792.00	66922.00	92474.00	87125.00	93520.00	30026.00	20507.00	18531.00	15943.00	88166.00	90541.00	2000.00	1607.00	31009.00	40631.00	85429.00	656	916	6	1	2019-07-18 15:16:09	2019-07-18 15:16:09	3730.00	4811.00	1015.00	87151.00	82393.00	79043.00
730	2075/76	3	9	9	3	9	10	2	6	7	1	20	46238.00	26671.00	25074.00	43981.00	35028.00	26200.00	73449.00	21693.00	72135.00	69087.00	42077.00	63030.00	98490.00	48604.00	99003.00	53582.00	21867.00	99950.00	93125.00	97585.00	631	1069	1	1	2019-07-18 15:16:09	2019-07-18 15:16:09	85202.00	10156.00	16133.00	69798.00	55050.00	78259.00
731	2075/76	3	5	5	2	1	3	9	1	3	5	19	90286.00	47462.00	70249.00	17313.00	1247.00	57862.00	94892.00	35643.00	19912.00	9568.00	92740.00	99577.00	70096.00	13969.00	18589.00	94479.00	83668.00	55861.00	58493.00	38104.00	450	1062	9	1	2019-07-18 15:16:09	2019-07-18 15:16:09	57337.00	51086.00	50558.00	24833.00	57594.00	66699.00
732	2075/76	3	8	10	6	1	10	9	10	1	10	12	5931.00	63640.00	3452.00	22458.00	15584.00	53499.00	21908.00	53490.00	70728.00	38174.00	40272.00	46661.00	57162.00	53004.00	46099.00	58222.00	95316.00	57590.00	65580.00	20725.00	339	1006	4	1	2019-07-18 15:16:09	2019-07-18 15:16:09	56917.00	74113.00	37583.00	15103.00	14679.00	53509.00
\.


--
-- TOC entry 2907 (class 0 OID 16878)
-- Dependencies: 209
-- Data for Name: offices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.offices (id, coperative_name, mobile_no, coperative_type, pan_no, area_id, province, district, palika, palika_type, ward_no, created_at, updated_at) FROM stdin;
892	साना किसान कृषि स. सं लि. बकधुवा, सप्तरी	\N	1	301600248	10	प्रदेश २	सप्तरी	अग्निसाइर कृष्ण सवरन गाउँपालिका	4	6	2019-07-18 15:42:30	2019-07-18 15:42:30
893	साना किसान कृषि स. सं लि. नकटिरायपुर, सप्तरी	\N	1	304800362	10	प्रदेश २	सप्तरी	रुपनी गाउँपालिका	4	1	2019-07-18 15:42:30	2019-07-18 15:42:30
894	साना किसान कृषि स. सं लि. हरिहरपुर, सप्तरी	\N	1	605170920	10	प्रदेश २	सप्तरी	अग्निसाइर कृष्ण सवरन गाउँपालिका	4	3	2019-07-18 15:42:30	2019-07-18 15:42:30
895	साना किसान कृषि स. सं लि. तेरहौता, सप्तरी	\N	1	301732558	10	प्रदेश २	सप्तरी	रुपनी गाउँपालिका	4	0	2019-07-18 15:42:30	2019-07-18 15:42:30
896	साना किसान कृषि स. सं लि. सिस्वाबेल्ही, सप्तरी	\N	1	304338698	10	प्रदेश २	सप्तरी	खडक नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
897	साना किसान कृषि स. सं लि. पातो, सप्तरी	\N	1	304800481	10	प्रदेश २	सप्तरी	डाक्नेश्वरी नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
898	साना किसान कृषि स. सं लि. ओद्राहा, सप्तरी	\N	1	304800449	10	प्रदेश २	सप्तरी	सप्तकोशी नगरपालिका	3	8	2019-07-18 15:42:30	2019-07-18 15:42:30
899	साना किसान कृषि स. सं लि. छिन्नमस्ता, सप्तरी	\N	1	601049550	10	प्रदेश २	सप्तरी	छिन्नमस्ता गाउँपालिका	4	3	2019-07-18 15:42:30	2019-07-18 15:42:30
900	साना किसान कृषि स. सं लि. पिप्रापुर्व, सप्तरी	\N	1	602689939	10	प्रदेश २	सप्तरी	कन्चनरुप नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
901	साना किसान कृषि स. सं लि. घोघनपुर, सप्तरी	\N	1	602683973	10	प्रदेश २	सप्तरी	कन्चनरुप नगरपालिका	3	6	2019-07-18 15:42:30	2019-07-18 15:42:30
902	सरस्वती साना किसान कृषि स. सं लि. मल्हनिया, सप्तरी	\N	1	601505670	10	प्रदेश २	सप्तरी	बलान बिहुल गाउँपालिका	4	1	2019-07-18 15:42:30	2019-07-18 15:42:30
903	रामनगर साना किसान कृषि स. सं लि. रामनगर, सप्तरी	\N	1	601505535	10	प्रदेश २	सप्तरी	बलान बिहुल गाउँपालिका	4	5	2019-07-18 15:42:30	2019-07-18 15:42:30
904	साना किसान कृषि स. सं लि. सुन्दरपुर, उदयपुर	\N	1	601048197	10	प्रदेश १	उदयपुर	चौदण्डीगढी नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
905	साना किसान कृषि स. सं लि. देउरी, उदयपुर	\N	1	301733223	10	प्रदेश १	उदयपुर	त्रियुगा नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
906	महिला साना किसान कृषि स. सं लि. रामपुरठोकसिला, उदयपुर	\N	1	601511105	10	प्रदेश १	उदयपुर	बेलका नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
907	महिला साना किसान कृषि स. सं लि. तपेश्वरी, उदयपुर	\N	1	601511106	10	प्रदेश १	उदयपुर	बेलका नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
908	महिला साना किसान कृषि स. सं लि. रौता, उदयपुर	\N	1	606953113	10	प्रदेश १	उदयपुर	रौतामाई गाउँपालिका	4	7	2019-07-18 15:42:30	2019-07-18 15:42:30
909	महिला साना किसान कृषि स. सं लि. पंचावती, उदयपुर	\N	1	601505432	10	प्रदेश १	उदयपुर	उदयपुरगढी गाउँपालिका	4	6	2019-07-18 15:42:30	2019-07-18 15:42:30
910	साना किसान कृषि स. सं लि. विष्णुपुर, सिरहा	\N	1	304800780	10	प्रदेश २	सिराहा	विष्णुपुर गाउँपालिका	4	1	2019-07-18 15:42:30	2019-07-18 15:42:30
911	साना किसान कृषि स. सं लि. लक्ष्मीपुर, सिरहा	\N	1	304800137	10	प्रदेश २	सिराहा	विष्णुपुर गाउँपालिका	4	5	2019-07-18 15:42:30	2019-07-18 15:42:30
912	साना किसान कृषि स. सं लि. महनौर, सिरहा	\N	1	304800584	10	प्रदेश २	सिराहा	अर्नमा गाउँपालिका	4	1	2019-07-18 15:42:30	2019-07-18 15:42:30
913	श्री बालासुन्दरी साना किसान कृषि स. सं लि. भगवानपुर, सिरहा	\N	1	304800603	10	प्रदेश २	सिराहा	भगवानपुर गाउँपालिका	4	1	2019-07-18 15:42:30	2019-07-18 15:42:30
914	साना किसान कृषि स. सं लि. पडरिया, सिरहा	\N	1	303918729	10	प्रदेश २	सिराहा	लहान नगरपालिका	3	11	2019-07-18 15:42:30	2019-07-18 15:42:30
915	साना किसान कृषि स. सं लि. बस्तीपुर, सिरहा	\N	1	303918433	10	प्रदेश २	सिराहा	लहान नगरपालिका	3	13	2019-07-18 15:42:30	2019-07-18 15:42:30
916	साना किसान कृषि स. सं लि. कल्याणपुर, सिरहा	\N	1	601252918	10	प्रदेश २	सिराहा	कल्याणपुर नगरपालिका	3	11	2019-07-18 15:42:30	2019-07-18 15:42:30
917	साना किसान कृषि स. सं लि. हनुमाननगर, सिरहा	\N	1	600869621	10	प्रदेश २	सिराहा	सिराहा नगरपालिका	3	11	2019-07-18 15:42:30	2019-07-18 15:42:30
918	साना किसान कृषि स. सं लि. भवानीपुर, सिरहा	\N	1	305412018	10	प्रदेश २	सिराहा	धनगढीमाई नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
919	साना किसान कृषि स. सं लि. असनपुर, सिरहा	\N	1	304809983	10	प्रदेश २	सिराहा	गोलबजार नगरपालिका	3	6	2019-07-18 15:42:30	2019-07-18 15:42:30
920	श्री लक्ष्मीनारायण साना किसान कृषि स. सं लि. नहरारिगौल, सिरहा	\N	1	601509476	10	प्रदेश २	सिराहा	भगवानपुर गाउँपालिका	4	2	2019-07-18 15:42:30	2019-07-18 15:42:30
921	माँ भवानी साना किसान कृषि स. सं लि. सितापुर, सिरहा	\N	1	601911930	10	प्रदेश २	सिराहा	लक्ष्मीपुर पतारी गाउँपालिका	4	6	2019-07-18 15:42:30	2019-07-18 15:42:30
922	आकाशगंगा साना किसान कृषि स. सं लि. मुक्सार, सिरहा	\N	1	600867908	10	प्रदेश २	सिराहा	गोलबजार नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
923	साना किसान कृषि स. सं लि. फुलकाहाकट्टी, सिरहा	\N	1	602836896	10	प्रदेश २	सिराहा	धनगढीमाई नगरपालिका	3	10	2019-07-18 15:42:30	2019-07-18 15:42:30
924	कुम्भीखाँडी साना किसान कृषि स. सं लि. लालपुर, सिरहा	\N	1	600867892	10	प्रदेश २	सिराहा	गोलबजार नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
925	धनगढीमाई साना किसान कृषि स. सं लि. धनगढी, सिरहा	\N	1	606222721	10	प्रदेश २	सिराहा	धनगढीमाई नगरपालिका	3	10	2019-07-18 15:42:30	2019-07-18 15:42:30
926	सफल महिला साना किसान कृषि स. सं लि. बेलहा, सिरहा	\N	1	606213718	10	प्रदेश २	सिराहा	कल्याणपुर नगरपालिका	3	12	2019-07-18 15:42:30	2019-07-18 15:42:30
927	अग्रशिल महिला साना किसान कृषि स. सं लि. अर्नमा, सिरहा	\N	1	606214100	10	प्रदेश २	सिराहा	कल्याणपुर नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
928	साना किसान कृषि स. सं लि. नकटाझिज, धनुषा	\N	1	304967023	10	प्रदेश २	धनुषा	मिथिला नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
929	साना किसान कृषि स. सं लि. दिगम्बरपुर, धनुषा	\N	1	304966763	10	प्रदेश २	धनुषा	क्षिरेश्वरनाथ नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
930	साना किसान कृषि स. सं लि. शान्तिपुर, धनुषा	\N	1	304966538	10	प्रदेश २	धनुषा	बटेश्वर गाउँपालिका	4	5	2019-07-18 15:42:30	2019-07-18 15:42:30
931	साना किसान कृषि स. सं लि. हरिहरपुर, धनुषा	\N	1	304966718	10	प्रदेश २	धनुषा	क्षिरेश्वरनाथ नगरपालिका	3	7	2019-07-18 15:42:30	2019-07-18 15:42:30
932	साना किसान कृषि स. सं लि. रघुनाथपुर, धनुषा	\N	1	304965728	10	प्रदेश २	धनुषा	सबैला नगरपालिका	3	12	2019-07-18 15:42:30	2019-07-18 15:42:30
933	साना किसान कृषि स. सं लि. सिंग्याहिमडान, धनुषा	\N	1	304966811	10	प्रदेश २	धनुषा	कमला नगरपालिका	3	6	2019-07-18 15:42:30	2019-07-18 15:42:30
934	साना किसान कृषि स. सं लि. मिथिलेश्वर, धनुषा	\N	1	304966628	10	प्रदेश २	धनुषा	मिथिला बिहारी नगरपालिका	3	3	2019-07-18 15:42:30	2019-07-18 15:42:30
935	साना किसान कृषि स. सं लि. ध. गोविन्दपुर, धनुषा	\N	1	304966399	10	प्रदेश २	धनुषा	धनुषाधाम नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
936	साना किसान कृषि स. सं लि. फुलगामा, धनुषा	\N	1	304966686	10	प्रदेश २	धनुषा	नगराइन नगरपालिका	3	4	2019-07-18 15:42:30	2019-07-18 15:42:30
937	साना किसान कृषि स. सं लि. गिद्धा, धनुषा	\N	1	304969148	10	प्रदेश २	धनुषा	बिदेह नगरपालिका	3	6	2019-07-18 15:42:30	2019-07-18 15:42:30
938	साना किसान कृषि स. सं लि. भरतपुर, धनुषा	\N	1	304966837	10	प्रदेश २	धनुषा	गणेशमान चारनाथ नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
939	साना किसान कृषि स. सं लि. पुष्पवलपुर, धनुषा	\N	1	304966789	10	प्रदेश २	धनुषा	मिथिला नगरपालिका	3	4	2019-07-18 15:42:30	2019-07-18 15:42:30
940	साना किसान कृषि स. सं लि. धनुषाधाम, धनुषा	\N	1	304966448	10	प्रदेश २	धनुषा	धनुषाधाम नगरपालिका	3	3	2019-07-18 15:42:30	2019-07-18 15:42:30
941	साना किसान कृषि स. सं लि. स. महेन्द्रनगर, धनुषा	\N	1	304966824	10	प्रदेश २	धनुषा	क्षिरेश्वरनाथ नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
942	साना किसान कृषि स. सं लि. नौवाखोर, धनुषा	\N	1	602222099	10	प्रदेश २	धनुषा	हंसपुर नगरपालिका	3	7	2019-07-18 15:42:30	2019-07-18 15:42:30
943	साना किसान कृषि स. सं लि. झटियाहि, धनुषा	\N	1	602222044	10	प्रदेश २	धनुषा	हंसपुर नगरपालिका	3	8	2019-07-18 15:42:30	2019-07-18 15:42:30
944	महिला साना किसान कृषि स. सं लि. बालाबाखर, धनुषा	\N	1	603047701	10	प्रदेश २	धनुषा	शहीदनगर नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
945	महिला साना किसान कृषि स. सं लि. सतोषर, धनुषा	\N	1	603047695	10	प्रदेश २	धनुषा	सबैला नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
946	महिला साना किसान कृषि स. सं लि. औरही, धनुषा	\N	1	604115979	10	प्रदेश २	धनुषा	औरही गाउँपालिका	4	4	2019-07-18 15:42:30	2019-07-18 15:42:30
947	महिला साना किसान कृषि स. सं लि. चक्कर, धनुषा	\N	1	604116541	10	प्रदेश २	धनुषा	औरही गाउँपालिका	4	5	2019-07-18 15:42:30	2019-07-18 15:42:30
948	महिला साना किसान कृषि स. सं लि. सुगानिकास, धनुषा	\N	1	606204390	10	प्रदेश २	धनुषा	हंसपुर नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
949	साना किसान कृषि स. सं लि. बघचौडा, धनुषा	\N	1	605154399	10	प्रदेश २	धनुषा	हंसपुर नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
950	महिला साना किसान कृषि स. सं लि. सोनिगामा, धनुषा	\N	1	606204213	10	प्रदेश २	धनुषा	हंसपुर नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
951	साना किसान कृषि स. सं लि. हंसपुरकठपुल्ला, धनुषा	\N	1	605153910	10	प्रदेश २	धनुषा	हंसपुर नगरपालिका	3	3	2019-07-18 15:42:30	2019-07-18 15:42:30
952	श्री महिला साना किसान कृषि स. सं. लि. हठीपुर हरवडा, धनुषा	\N	1	606206325	10	प्रदेश २	धनुषा	शहीदनगर नगरपालिका	3	6	2019-07-18 15:42:30	2019-07-18 15:42:30
953	साना किसान कृषि स. सं लि. बिसनपुर, महोत्तरी	\N	1	304969447	10	प्रदेश २	महोत्तरी	मटिहानी नगरपालिका	3	4	2019-07-18 15:42:30	2019-07-18 15:42:30
954	साना किसान कृषि स. सं लि. इटहर्वाकट्टी, महोत्तरी	\N	1	603890130	10	प्रदेश २	महोत्तरी	मनरा शिसवा नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
955	साना किसान कृषि स. सं लि. बन्चौरी, महोत्तरी	\N	1	304966792	10	प्रदेश २	महोत्तरी	बलवा नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
956	साना किसान कृषि स. सं लि. रामगोपालपुर, महोत्तरी	\N	1	600819954	10	प्रदेश २	महोत्तरी	राम गोपालपुर नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
957	साना किसान कृषि स. सं लि. हल्खोरी, महोत्तरी	\N	1	608949857	10	प्रदेश २	महोत्तरी	एकडारा गाउँपालिका	4	4	2019-07-18 15:42:30	2019-07-18 15:42:30
958	साना किसान कृषि स. सं लि. धिरापुर, महोत्तरी	\N	1	304966895	10	प्रदेश २	महोत्तरी	मटिहानी नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
959	साना किसान कृषि स. सं लि. भंगहा, महोत्तरी	\N	1	304966422	10	प्रदेश २	महोत्तरी	भँगाहा नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
960	साना किसान कृषि स. सं लि. सिंग्याहि, महोत्तरी	\N	1	305681382	10	प्रदेश २	महोत्तरी	भँगाहा नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
961	साना किसान कृषि स. सं लि. हरिणमरी, महोत्तरी	\N	1	601203646	10	प्रदेश २	महोत्तरी	भँगाहा नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
962	साना किसान कृषि स. सं लि. खुट्टापिपराढी, महोत्तरी	\N	1	604775197	10	प्रदेश २	महोत्तरी	लोहरपट्टी नगरपालिका	3	4	2019-07-18 15:42:30	2019-07-18 15:42:30
963	साना किसान कृषि स. सं लि. जब्दी, सर्लाही	\N	1	304965937	10	प्रदेश २	सर्लाही	लालबन्दी नगरपालिका	3	4	2019-07-18 15:42:30	2019-07-18 15:42:30
964	साना किसान कृषि स. सं लि. पिडारी, सर्लाही	\N	1	304537749	10	प्रदेश २	सर्लाही	हरिपुर नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
965	साना किसान कृषि स. सं लि. बगदह, सर्लाही	\N	1	304966239	10	प्रदेश २	सर्लाही	गोडैटा नगरपालिका	3	9	2019-07-18 15:42:30	2019-07-18 15:42:30
966	साना किसान कृषि स. सं लि. हरिपुर्वा, सर्लाही	\N	1	304966464	10	प्रदेश २	सर्लाही	हरिपुर्वा नगरपालिका	3	5	2019-07-18 15:42:30	2019-07-18 15:42:30
967	साना किसान कृषि स. सं लि. शंकरपुर, सर्लाही	\N	1	302870264	10	प्रदेश २	सर्लाही	बागमती नगरपालिका	3	10	2019-07-18 15:42:30	2019-07-18 15:42:30
968	साना किसान कृषि स. सं लि. नौकेलवा, सर्लाही	\N	1	304966699	10	प्रदेश २	सर्लाही	ब्रह्मपुरी गाउँपालिका	4	7	2019-07-18 15:42:30	2019-07-18 15:42:30
969	साना किसान कृषि स. सं लि. लक्ष्मीपुर, सर्लाही	\N	1	303242446	10	प्रदेश २	सर्लाही	हरिपुर नगरपालिका	3	6	2019-07-18 15:42:30	2019-07-18 15:42:30
970	साना किसान कृषि स. सं लि. हरिपुर, सर्लाही	\N	1	304209657	10	प्रदेश २	सर्लाही	हरिपुर नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
971	साना किसान कृषि स. सं लि. भक्तिपुर, सर्लाही	\N	1	304966110	10	प्रदेश २	सर्लाही	ईश्वरपुर नगरपालिका	3	3	2019-07-18 15:42:30	2019-07-18 15:42:30
972	साना किसान कृषि स. सं लि. गौरीशंकर, सर्लाही	\N	1	304534904	10	प्रदेश २	सर्लाही	ईश्वरपुर नगरपालिका	3	14	2019-07-18 15:42:30	2019-07-18 15:42:30
973	महिला साना किसान कृषि स. सं लि. परवानीपुर, सर्लाही	\N	1	601007697	10	प्रदेश २	सर्लाही	लालबन्दी नगरपालिका	3	16	2019-07-18 15:42:30	2019-07-18 15:42:30
974	महिला साना किसान कृषि स. सं लि. रानीगंज, सर्लाही	\N	1	601008005	10	प्रदेश २	सर्लाही	लालबन्दी नगरपालिका	3	17	2019-07-18 15:42:30	2019-07-18 15:42:30
975	श्री राम जानकी साना किसान कृषि स. सं लि. ढुंग्रेखोला, सर्लाही	\N	1	603217975	10	प्रदेश २	सर्लाही	बागमती नगरपालिका	3	2	2019-07-18 15:42:30	2019-07-18 15:42:30
976	साना किसान कृषि स. सं लि. कर्मैया, सर्लाही	\N	1	305548089	10	प्रदेश २	सर्लाही	बागमती नगरपालिका	3	12	2019-07-18 15:42:30	2019-07-18 15:42:30
977	सर्वोदय साना किसान कृषि स. सं लि. कौडेना, सर्लाही	\N	1	606107727	10	प्रदेश २	सर्लाही	कौडेना गाउँपालिका	4	3	2019-07-18 15:42:30	2019-07-18 15:42:30
978	महिला साना किसान कृषि स. सं लि. कालिन्जोर, सर्लाही	\N	1	605510056	10	प्रदेश २	सर्लाही	ईश्वरपुर नगरपालिका	3	12	2019-07-18 15:42:30	2019-07-18 15:42:30
979	श्री जानकी साना किसान कृषि स. सं लि. भेलही, सर्लाही	\N	1	605428658	10	प्रदेश २	सर्लाही	ब्रह्मपुरी गाउँपालिका	4	3	2019-07-18 15:42:30	2019-07-18 15:42:30
980	श्री कमलामाई महिला साना किसान कृषि सहकारी सं. लि. दुधौली, सिन्धुली	\N	1	601656536	10	प्रदेश ३	सिन्धुली	कमलामाई नगरपालिका	3	10	2019-07-18 15:42:30	2019-07-18 15:42:30
981	महिला साना किसान कृषि सहकारी सं. लि. रानीबास, सिन्धुली	\N	1	601656549	10	प्रदेश ३	सिन्धुली	कमलामाई नगरपालिका	3	14	2019-07-18 15:42:30	2019-07-18 15:42:30
982	हातेमालो साना किसान कृषि सहकारी सं. लि. डाडिगुँरासे, सिन्धुली	\N	1	601004698	10	प्रदेश ३	सिन्धुली	कमलामाई नगरपालिका	3	1	2019-07-18 15:42:30	2019-07-18 15:42:30
\.


--
-- TOC entry 2898 (class 0 OID 16828)
-- Dependencies: 200
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2897 (class 0 OID 16813)
-- Dependencies: 199
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, email, email_verified_at, password, role, status, office_id, remember_token, created_at, updated_at) FROM stdin;
1121	manakamana	manakamana@gmail.com	\N	$2y$10$FuFA108Pfj3fLpGr2ZRxUeUsvirhS8EEYiP8MFT6.pLYdO603eCDi	2	1	\N	\N	2019-07-18 15:16:09	2019-07-18 15:16:09
1122	aguiliar5	aguilar95@gmail.com	\N	test123	0	1	892	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1123	aguiliar6	\N	\N	test124	0	1	893	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1124	aguiliar7	\N	\N	test125	0	1	894	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1125	aguiliar8	\N	\N	test126	0	1	895	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1126	aguiliar9	\N	\N	test127	0	1	896	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1127	aguiliar10	\N	\N	test128	0	1	898	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1128	aguiliar11	\N	\N	test129	0	1	899	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1129	aguiliar12	\N	\N	test130	0	1	900	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1130	aguiliar13	\N	\N	test131	0	1	901	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1131	aguiliar14	\N	\N	test132	0	1	902	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1132	aguiliar15	\N	\N	test133	0	1	903	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1133	aguiliar16	\N	\N	test134	0	1	904	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1134	aguiliar17	\N	\N	test135	0	1	905	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1135	aguiliar18	\N	\N	test136	0	1	906	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1136	aguiliar19	\N	\N	test137	0	1	907	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1137	aguiliar20	\N	\N	test138	0	1	908	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1138	aguiliar21	\N	\N	test139	0	1	909	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1139	aguiliar22	\N	\N	test140	0	1	910	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1140	aguiliar23	\N	\N	test141	0	1	911	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1141	aguiliar24	\N	\N	test142	0	1	912	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1142	aguiliar25	\N	\N	test143	0	1	913	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
1143	aguiliar26	\N	\N	test144	0	1	914	\N	2019-07-18 15:56:12	2019-07-18 15:56:12
\.


--
-- TOC entry 2927 (class 0 OID 0)
-- Dependencies: 212
-- Name: area_office_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.area_office_id_seq', 11, true);


--
-- TOC entry 2928 (class 0 OID 0)
-- Dependencies: 196
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 11, true);


--
-- TOC entry 2929 (class 0 OID 0)
-- Dependencies: 204
-- Name: oauth_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_clients_id_seq', 1, false);


--
-- TOC entry 2930 (class 0 OID 0)
-- Dependencies: 206
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_personal_access_clients_id_seq', 1, false);


--
-- TOC entry 2931 (class 0 OID 0)
-- Dependencies: 210
-- Name: office_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.office_data_id_seq', 732, true);


--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 208
-- Name: offices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.offices_id_seq', 982, true);


--
-- TOC entry 2933 (class 0 OID 0)
-- Dependencies: 198
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1143, true);


--
-- TOC entry 2772 (class 2606 OID 16912)
-- Name: area_office area_office_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_office
    ADD CONSTRAINT area_office_pkey PRIMARY KEY (id);


--
-- TOC entry 2743 (class 2606 OID 16810)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2752 (class 2606 OID 16847)
-- Name: oauth_access_tokens oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- TOC entry 2750 (class 2606 OID 16839)
-- Name: oauth_auth_codes oauth_auth_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_auth_codes
    ADD CONSTRAINT oauth_auth_codes_pkey PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 16865)
-- Name: oauth_clients oauth_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (id);


--
-- TOC entry 2762 (class 2606 OID 16874)
-- Name: oauth_personal_access_clients oauth_personal_access_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_personal_access_clients
    ADD CONSTRAINT oauth_personal_access_clients_pkey PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 16853)
-- Name: oauth_refresh_tokens oauth_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_refresh_tokens
    ADD CONSTRAINT oauth_refresh_tokens_pkey PRIMARY KEY (id);


--
-- TOC entry 2770 (class 2606 OID 16904)
-- Name: office_data office_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.office_data
    ADD CONSTRAINT office_data_pkey PRIMARY KEY (id);


--
-- TOC entry 2764 (class 2606 OID 16889)
-- Name: offices offices_mobile_no_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices
    ADD CONSTRAINT offices_mobile_no_unique UNIQUE (mobile_no);


--
-- TOC entry 2766 (class 2606 OID 16891)
-- Name: offices offices_pan_no_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices
    ADD CONSTRAINT offices_pan_no_unique UNIQUE (pan_no);


--
-- TOC entry 2768 (class 2606 OID 16887)
-- Name: offices offices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices
    ADD CONSTRAINT offices_pkey PRIMARY KEY (id);


--
-- TOC entry 2745 (class 2606 OID 16827)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2747 (class 2606 OID 16825)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2753 (class 1259 OID 16848)
-- Name: oauth_access_tokens_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_access_tokens_user_id_index ON public.oauth_access_tokens USING btree (user_id);


--
-- TOC entry 2759 (class 1259 OID 16866)
-- Name: oauth_clients_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_clients_user_id_index ON public.oauth_clients USING btree (user_id);


--
-- TOC entry 2760 (class 1259 OID 16875)
-- Name: oauth_personal_access_clients_client_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_personal_access_clients_client_id_index ON public.oauth_personal_access_clients USING btree (client_id);


--
-- TOC entry 2754 (class 1259 OID 16854)
-- Name: oauth_refresh_tokens_access_token_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_refresh_tokens_access_token_id_index ON public.oauth_refresh_tokens USING btree (access_token_id);


--
-- TOC entry 2748 (class 1259 OID 16831)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


-- Completed on 2019-07-18 16:41:11

--
-- PostgreSQL database dump complete
--
