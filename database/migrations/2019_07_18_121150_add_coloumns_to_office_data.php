<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumnsToOfficeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('office_data', function($table) {
          $table->decimal('paunu_parne__baki_byaj', 12, 2)->nullable();
          $table->decimal('byaj_income', 12, 2)->nullable();
          $table->decimal('other_income', 12, 2)->nullable();
          $table->decimal('byaj_expense', 12, 2)->nullable();
          $table->decimal('karmachari_parsasanik_expense', 12, 2)->nullable();
          $table->decimal('jokhim_kosh_expense', 12, 2)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_data');
    }
}
