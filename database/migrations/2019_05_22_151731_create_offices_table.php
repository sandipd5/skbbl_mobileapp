<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('coperative_name');
          $table->string('mobile_no')->unique()->nullable();
          $table->enum('coperative_type',['1','2']);//1=SFCL,2=others
          $table->string('pan_no')->unique();
          $table->integer('area_id');
          $table->string('province');
          $table->string('district');
          $table->string('palika');
          $table->string('palika_type');
          $table->integer('ward_no');
          $table->timestamps();

        });
    }
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
