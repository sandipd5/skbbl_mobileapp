<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_data', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('fiscal_year');
          $table->integer('month_id');

          $table->integer('member_dalit_male')->nullable();
          $table->integer('member_dalit_female')->nullable();
          $table->integer('member_janajati_male')->nullable();
          $table->integer('member_janajati_female')->nullable();
          $table->integer('member_other_male')->nullable();
          $table->integer('member_other_female')->nullable();
          $table->integer('total_member')->nullable();

          $table->integer('loaned_male_number')->nullable();
          $table->integer('loaned_female_number')->nullable();
          $table->integer('total_loaned_people')->nullable();

          $table->decimal('laganima_raheko_rakam', 12, 2)->nullable();
          $table->decimal('lagani', 12, 2)->nullable();
          $table->decimal('sewa_asuli', 12, 2)->nullable();
          $table->decimal('byaj_asuli', 12, 2)->nullable();
          $table->decimal('vaka_nageko_rakam', 12, 2)->nullable();

          $table->decimal('antarik_source_share_pungi', 12, 2)->nullable();
          $table->decimal('antarik_source_jageda_kosh', 12, 2)->nullable();
          $table->decimal('antarik_source_samuha_bachat', 12, 2)->nullable();
          $table->decimal('antarik_source_other', 12, 2)->nullable();
          $table->decimal('total_antarik_source_pungi', 12, 2)->nullable();


          $table->decimal('skbbl_bata_lieko_loan',12,2)->nullable();
          $table->decimal('skbbl_lai_bujaeko_loan',12,2)->nullable();
          $table->decimal('skbbl_lai_tirna_baki_loan',12,2)->nullable();

          $table->decimal('aru_bata_lieko_loan', 12, 2)->nullable();
          $table->decimal('aru_lai_bujaeko_loan', 12, 2)->nullable();
          $table->decimal('aru_lai_tirna_baki_loan', 12, 2)->nullable();

          $table->decimal('total_external_loan', 12, 2)->nullable();

          $table->decimal('total_income', 12, 2)->nullable();
          $table->decimal('total_expense', 12, 2)->nullable();
          $table->decimal('profit_loss', 12, 2)->nullable();

          $table->unsignedInteger('office_id');
          $table->unsignedInteger('user_id');
          $table->unsignedInteger('area_id');
          $table->enum('status',['1','0'])->default('1');//1=active,0=inactive
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_data');
    }
}
