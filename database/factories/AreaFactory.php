<?php

use Faker\Generator as Faker;

$factory->define(App\Area::class, function (Faker $faker) {
    return [
      'name' => $faker->unique()->randomElement(['Birtamod','attariya','Nepalgunj','Butwal',
                  'pokhara','Itahari','Janakpur','Hetauda','Gajuri','Birendranagar']),
      'code' => $faker->unique()->numberBetween($min = 1, $max = 10),
    ];
});
