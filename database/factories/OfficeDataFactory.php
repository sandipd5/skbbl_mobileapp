<?php

use Faker\Generator as Faker;

$factory->define(App\Officedata::class, function (Faker $faker) {
    return [
        'fiscal_year' => '2075/76',
        'month_id' => 3,
        'member_dalit_male' =>  $faker->numberBetween($min = 1, $max = 10),
        'member_dalit_female' =>  $faker->numberBetween($min = 1, $max = 10),
        'member_janajati_male' => $faker->numberBetween($min = 1, $max = 10),
        'member_janajati_female' => $faker->numberBetween($min = 1, $max = 10),
        'member_other_female' => $faker->numberBetween($min = 1, $max = 10),
        'total_member' => $faker->numberBetween($min = 1, $max = 10),
        'loaned_male_number' => $faker->numberBetween($min = 1, $max = 10),
        'loaned_female_number' =>  $faker->numberBetween($min = 1, $max = 10),
        'total_loaned_people' => $faker->numberBetween($min = 10, $max = 20),
        'member_other_male' => $faker->numberBetween($min = 1, $max = 10),
        'laganima_raheko_rakam' => $faker->numberBetween($min = 1000, $max = 100000),
        'lagani' =>$faker->numberBetween($min = 1000, $max = 100000),
        'sewa_asuli' => $faker->numberBetween($min = 1000, $max = 100000),
        'byaj_asuli' =>$faker->numberBetween($min = 1000, $max = 100000),
        'antarik_source_share_pungi'=> $faker->numberBetween($min = 1000, $max = 100000),
        'antarik_source_jageda_kosh'=> $faker->numberBetween($min = 1000, $max = 100000),
        'antarik_source_samuha_bachat'=> $faker->numberBetween($min = 1000, $max = 100000),
         'antarik_source_other'=> $faker->numberBetween($min = 1000, $max = 100000),
         'vaka_nageko_rakam' => $faker->numberBetween($min = 1000, $max = 100000),
          'paunu_parne__baki_byaj' => $faker->numberBetween($min = 1000, $max = 100000),

        'total_antarik_source_pungi' => $faker->numberBetween($min = 1000, $max = 100000),
        'skbbl_bata_lieko_loan' => $faker->numberBetween($min = 1000, $max = 100000),
        'skbbl_lai_bujaeko_loan'=> $faker->numberBetween($min = 1000, $max = 100000),
        'skbbl_lai_tirna_baki_loan'=> $faker->numberBetween($min = 1000, $max = 100000),
        'aru_bata_lieko_loan' => $faker->numberBetween($min = 1000, $max = 100000),
        'aru_lai_bujaeko_loan'=> $faker->numberBetween($min = 1000, $max = 100000),
         'aru_lai_tirna_baki_loan'=> $faker->numberBetween($min = 1000, $max = 100000),
        'total_external_loan' =>  $faker->numberBetween($min = 1000, $max = 100000),
        'byaj_income' => $faker->numberBetween($min = 1000, $max = 100000),
        'other_income' => $faker->numberBetween($min = 1000, $max = 100000),
        'total_income' => $faker->numberBetween($min = 1000, $max = 100000),
        'byaj_expense' => $faker->numberBetween($min = 1000, $max = 100000),
        'karmachari_parsasanik_expense' => $faker->numberBetween($min = 1000, $max = 100000),
        'jokhim_kosh_expense' => $faker->numberBetween($min = 1000, $max = 100000),
        'total_expense' => $faker->numberBetween($min = 1000, $max = 100000),
        'profit_loss' => $faker->numberBetween($min = 1000, $max = 100000),
        'office_id' =>$faker->unique()->numberBetween($min = 201, $max = 700),
        'user_id'=> $faker->unique()->numberBetween($min = 602, $max = 1101),
        'area_id'=> $faker->numberBetween($min = 1, $max = 10),



    ];
});
