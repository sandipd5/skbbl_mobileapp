<?php

use Faker\Generator as Faker;

$factory->define(App\Office::class, function (Faker $faker) {
    return [
        'coperative_name' => $faker->sentence(10),
        'coperative_type' => $faker->randomElement(['1', '2']),
        'pan_no' =>  $faker->unique()->numberBetween($min = 1111, $max = 99999),
        'province' => $faker->randomElement(['karnali','pradesh 1','pradesh 2','gandaki','pradesh 3','pradesh 7']),
        'district' => $faker->randomElement(['Saptari','Jhapa','Udayapur','Palpa','Pyuthan','Salyan','Jumla',
                                            'Dailekh','Doti','Bajhang','Baitadi','Dadeldhura']),
        'palika' =>  $faker->randomElement(['Saptari','Jhapa','Udayapur','Palpa','Pyuthan','Salyan','Jumla',
                                            'Dailekh','Doti','Bajhang','Baitadi','Dadeldhura']),
        'palika_type' => $faker->randomElement(['1','2','3','4']),
        'ward_no' => $faker->randomNumber($nbDigits = NULL, $strict = false) ,
        'area_id' => 11,


    ];
});
