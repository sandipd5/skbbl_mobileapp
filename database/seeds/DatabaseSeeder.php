<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Office;
use App\OfficeData;
use App\Area;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
          //$this->call(UserTableSeeder::class);
          // $this->call(AreaTableSeeder::class);
        // Area::create(['name' => 'Unit','code' => 11]);
      User::create(['username' => 'manakamana', 'email' => 'manakamana@gmail.com','role' => '2','password' => bcrypt('admin123')]);
        /*Sansthainfo::create(['coperative_name'=>'demo sana kisan sahakari','username'=>'demoskbbluser',
                                'password'=> bcrypt('admin1'),'mobile_no'=>'9861585016','coperative_type'=>'1',
                                'pan_no'=>'54754574','area_office'=>'birtamod',
                                'province'=>'karnali','district'=>'morang','palika'=>'biratnagar',
                                'palika_type'=>'nagar','ward_no'=>'1']);
          */
    /*  OfficeData::create(['fiscal_year'=>'2019','month'=>'baisakh','member_dalit_male'=>5,'member_dalit_female'=>1
        ,'member_janajati_male'=>20,'member_janajati_female'=>46,'member_other_male'=>28,'member_other_female'=>30,
        'total_member'=>4,'loaned_male_number'=>8,'loaned_female_number'=>5,'total_loaned_people'=>12,
           'laganima_raheko_rakam'=>1000,'lagani'=>333333,'sewa_asuli'=>34343,'byaj_asuli'=>2323,
           'vaka_nageko_rakam'=>3211,'antarik_source_share_pungi'=>121,'antarik_source_jageda_kosh'=>1211,
           'antarik_source_samuha_bachat'=>2111, 'antarik_source_other'=>54453,
           'total_antarik_source_pungi'=>42232,'skbbl_bata_lieko_tirna_baki_loan'=>645443,
           'aru_bata_lieko_tirna_baki_loan'=>5988,'total_external_loan'=>7690,
             'total_income'=>43442,'total_expense'=>86776,'profit_loss'=>76665,
             'office_id'=>3,'user_id'=>4]);

       */
      $this->call(OfficeDataTableSeeder::class);
       //$this->call(OfficeTableSeeder::class);
    //$this->call(AreaTableSeeder::class);

    }
}
