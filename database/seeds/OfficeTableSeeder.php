<?php

use Illuminate\Database\Seeder;

class OfficeTableSeeder extends Seeder
{

    public function run()
    {
        factory(App\Office::class,40)->create();
    }
}
