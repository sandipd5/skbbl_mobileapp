<?php

use Illuminate\Database\Seeder;

class OfficeDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Officedata::class,10)->create();
    }
}
