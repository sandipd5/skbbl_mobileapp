-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 22, 2020 at 09:38 AM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_skbblapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_offices`
--

CREATE TABLE `area_offices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `area_offices`
--

INSERT INTO `area_offices` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'btl', '1', NULL, NULL),
(2, 'admin', '2', NULL, NULL),
(3, 'pkr', '3', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_05_22_151731_create_offices_table', 1),
(9, '2019_05_22_163641_create_office_data_table', 1),
(10, '2019_05_27_122546_create_area_office_table', 1),
(11, '2019_07_18_121150_add_coloumns_to_office_data', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coperative_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coperative_type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `pan_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_office_id` int(11) NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `palika` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `palika_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ward_no` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `coperative_name`, `mobile_no`, `coperative_type`, `pan_no`, `area_office_id`, `province`, `district`, `palika`, `palika_type`, `ward_no`, `created_at`, `updated_at`) VALUES
(1, 'butwal coperative', NULL, '1', '4343', 1, 'vhvhm', 'b', 'vvv', 'nmm', 9, NULL, NULL),
(2, 'pokhara coperative\r\n', NULL, '1', '65655', 3, 'hdhd', 'hdjj', 'jgfjf', 'jfjfff', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `office_data`
--

CREATE TABLE `office_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fiscal_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `month` int(11) NOT NULL,
  `member_dalit_male` int(11) DEFAULT NULL,
  `member_dalit_female` int(11) DEFAULT NULL,
  `member_janajati_male` int(11) DEFAULT NULL,
  `member_janajati_female` int(11) DEFAULT NULL,
  `member_other_male` int(11) DEFAULT NULL,
  `member_other_female` int(11) DEFAULT NULL,
  `total_member` int(11) DEFAULT NULL,
  `loaned_male_number` int(11) DEFAULT NULL,
  `loaned_female_number` int(11) DEFAULT NULL,
  `total_loaned_people` int(11) DEFAULT NULL,
  `laganima_raheko_rakam` decimal(12,2) DEFAULT NULL,
  `lagani` decimal(12,2) DEFAULT NULL,
  `sewa_asuli` decimal(12,2) DEFAULT NULL,
  `byaj_asuli` decimal(12,2) DEFAULT NULL,
  `vaka_nageko_rakam` decimal(12,2) DEFAULT NULL,
  `antarik_source_share_pungi` decimal(12,2) DEFAULT NULL,
  `antarik_source_jageda_kosh` decimal(12,2) DEFAULT NULL,
  `antarik_source_samuha_bachat` decimal(12,2) DEFAULT NULL,
  `antarik_source_other` decimal(12,2) DEFAULT NULL,
  `total_antarik_source_pungi` decimal(12,2) DEFAULT NULL,
  `skbbl_bata_lieko_loan` decimal(12,2) DEFAULT NULL,
  `skbbl_lai_bujaeko_loan` decimal(12,2) DEFAULT NULL,
  `skbbl_lai_tirna_baki_loan` decimal(12,2) DEFAULT NULL,
  `aru_bata_lieko_loan` decimal(12,2) DEFAULT NULL,
  `aru_lai_bujaeko_loan` decimal(12,2) DEFAULT NULL,
  `aru_lai_tirna_baki_loan` decimal(12,2) DEFAULT NULL,
  `total_external_loan` decimal(12,2) DEFAULT NULL,
  `total_income` decimal(12,2) DEFAULT NULL,
  `total_expense` decimal(12,2) DEFAULT NULL,
  `profit_loss` decimal(12,2) DEFAULT NULL,
  `office_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `area_office_id` int(10) UNSIGNED NOT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paunu_parne__baki_byaj` decimal(12,2) DEFAULT NULL,
  `byaj_income` decimal(12,2) DEFAULT NULL,
  `other_income` decimal(12,2) DEFAULT NULL,
  `byaj_expense` decimal(12,2) DEFAULT NULL,
  `karmachari_parsasanik_expense` decimal(12,2) DEFAULT NULL,
  `jokhim_kosh_expense` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `office_data`
--

INSERT INTO `office_data` (`id`, `fiscal_year`, `month`, `member_dalit_male`, `member_dalit_female`, `member_janajati_male`, `member_janajati_female`, `member_other_male`, `member_other_female`, `total_member`, `loaned_male_number`, `loaned_female_number`, `total_loaned_people`, `laganima_raheko_rakam`, `lagani`, `sewa_asuli`, `byaj_asuli`, `vaka_nageko_rakam`, `antarik_source_share_pungi`, `antarik_source_jageda_kosh`, `antarik_source_samuha_bachat`, `antarik_source_other`, `total_antarik_source_pungi`, `skbbl_bata_lieko_loan`, `skbbl_lai_bujaeko_loan`, `skbbl_lai_tirna_baki_loan`, `aru_bata_lieko_loan`, `aru_lai_bujaeko_loan`, `aru_lai_tirna_baki_loan`, `total_external_loan`, `total_income`, `total_expense`, `profit_loss`, `office_id`, `user_id`, `area_office_id`, `status`, `created_at`, `updated_at`, `paunu_parne__baki_byaj`, `byaj_income`, `other_income`, `byaj_expense`, `karmachari_parsasanik_expense`, `jokhim_kosh_expense`) VALUES
(1, '2077/78', 5, 2, 5, 5, 7, 7, 4, 7, 7, 45, 4, '3.00', '4.00', '4.00', '6.00', '7.00', '8.00', '8.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '577.00', '678.00', '101.00', 1, 4, 1, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2077/78', 5, 1, 1, 1, 1, 1, 1, 3, 4, NULL, 5, '78.00', '76.00', '6.00', '6.00', '6.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50.00', '40.00', '10.00', '30.00', 2, 5, 3, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2077/78', 4, 5, 645, 3, 5, 43, NULL, 5, NULL, NULL, 5, NULL, '7.00', NULL, '8.00', NULL, '8.00', '9.00', '9.00', NULL, NULL, '9.00', NULL, NULL, '8.00', NULL, NULL, NULL, '50.00', '20.00', '30.00', 1, 4, 1, '1', NULL, '2020-09-05 18:15:00', '7.00', NULL, NULL, '6.00', NULL, NULL),
(4, '2077/78', 4, 0, 9, 6, NULL, NULL, NULL, 67, NULL, NULL, NULL, NULL, '6.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100.00', '10.00', '90.00', 2, 5, 3, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `office_id` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `role`, `status`, `office_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'skbbl@gmail.com', 'skbbl@gmail.com', NULL, '$2y$12$hJF04jsVwvtO85MSkuoDHOcFUFL.0ZrXnu8dl/FSTEZFZB4C.IcS.', '2', '1', NULL, 'tfNqTQ7Tzh8Q5uOkOA5u1NuUEcXBctFrAgQ1Y6NR4iuZA4SKwLMCQ2TktxGH', NULL, NULL),
(2, 'btl@gmail.com', 'btl@gmail.com', NULL, '$2y$12$hJF04jsVwvtO85MSkuoDHOcFUFL.0ZrXnu8dl/FSTEZFZB4C.IcS.', '1', '1', NULL, 'iTtq5spajcKb0xW7DFtrEeGYQKBTQx68qIPfTLOP55HlHThhnaC7LCbf6lm2', NULL, NULL),
(3, 'pkr@gmail.com', 'pkr@gmail.com', NULL, '$2y$12$hJF04jsVwvtO85MSkuoDHOcFUFL.0ZrXnu8dl/FSTEZFZB4C.IcS.', '3', '1', NULL, 'POtbj84aFWAUrZdwaEXyNyK80b6cpLX3ay3tIzQn7QbLa1cFAwkpPBnPPdPm', NULL, NULL),
(4, 'sfacl butwal', NULL, NULL, '$2y$12$hJF04jsVwvtO85MSkuoDHOcFUFL.0ZrXnu8dl/FSTEZFZB4C.IcS.', '0', '1', 1, NULL, NULL, NULL),
(5, 'SFCACL pokhara', NULL, NULL, '$2y$12$hJF04jsVwvtO85MSkuoDHOcFUFL.0ZrXnu8dl/FSTEZFZB4C.IcS.', '0', '1', 2, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area_offices`
--
ALTER TABLE `area_offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `offices_pan_no_unique` (`pan_no`),
  ADD UNIQUE KEY `offices_mobile_no_unique` (`mobile_no`);

--
-- Indexes for table `office_data`
--
ALTER TABLE `office_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area_offices`
--
ALTER TABLE `area_offices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `office_data`
--
ALTER TABLE `office_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
